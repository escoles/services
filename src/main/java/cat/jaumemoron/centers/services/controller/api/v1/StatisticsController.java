package cat.jaumemoron.centers.services.controller.api.v1;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import cat.jaumemoron.centers.persistence.service.StatisticsService;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.dto.ErrorResponseDTO;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.util.Converter;
import cat.jaumemoron.centers.services.util.PaginationUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.STATISTIC_REQUEST_MAPPING)
@Api(value = "Service to get statistics about system", tags = "Statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService service;

    @GetMapping(RequestMappingConstants.TYPE_PATH_PARAM)
    @ApiOperation(value = "Get statistics of a type", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Statistic> findByType(@ApiParam(value = "Statistic type", required = true)
                                         @PathVariable(RequestMappingConstants.TYPE) int type,
                                         @ApiParam(value = "Initial range")
                                         @RequestParam(value = "initDate", required = false) @DateTimeFormat(pattern = PersistenceConstants.DEFAULT_DATE_FORMAT) Date init,
                                         @ApiParam(value = "End range")
                                         @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = PersistenceConstants.DEFAULT_DATE_FORMAT) Date end,
                                         @ApiParam(value = "Zero-based element index", required = true)
                                         @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                         @ApiParam(value = "The size of the page to be returned", required = true)
                                         @RequestParam(RequestMappingConstants.SIZE_PARAM) int size) {
        PageRequest request = PaginationUtils.getPageRequest(start, size, Sort.by(Sort.Direction.ASC, "date"));
        if (type < 0 || StatisticType.values().length < type)
            throw new IllegalArgumentException("Statistic type [" + type + "] has an invalid value");

        if (init != null && end != null)
            return Converter.toStatisticPageDTO(service.findByType(StatisticType.values()[type], init, end, request));
        else
            return Converter.toStatisticPageDTO(service.findByType(StatisticType.values()[type], request));
    }
}
