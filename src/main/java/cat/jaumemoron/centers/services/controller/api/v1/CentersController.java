package cat.jaumemoron.centers.services.controller.api.v1;

import cat.jaumemoron.centers.persistence.domain.Center;
import cat.jaumemoron.centers.persistence.service.CenterService;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.dto.ErrorResponseDTO;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.exception.ResourceNotFoundException;
import cat.jaumemoron.centers.services.util.Converter;
import cat.jaumemoron.centers.services.util.PaginationUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.CENTER_REQUEST_MAPPING)
@Api(value = "Service to get data about centers", tags = "Centers")
public class CentersController {

    private static final int MINIMAL_TERM_LENGTH = 4;

    @Autowired
    private CenterService service;

    @GetMapping(value = RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get center by id", response = Center.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The center data", response = Center.class),
            @ApiResponse(code = 404, message = "No center was found", response = ErrorResponseDTO.class)}
    )
    public Center get(@ApiParam(value = "The center id", required = true)
                      @PathVariable(RequestMappingConstants.ID_PARAM) String id) {
        if (ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("Center id cannot be null");
        Optional<Center> optional = service.findById(id);
        if (!optional.isPresent())
            throw new ResourceNotFoundException("Center with id [" + id + "] does not exists");
        return optional.get();
    }

    private PageDTO<Center> buildResponse(UriComponentsBuilder uriBuilder, HttpServletResponse response,
                                          int size, Page<Center> page) {
        if (page.getNumberOfElements() > 0)
            response.addHeader("Link", PaginationUtils.getLinkHeader(uriBuilder,
                    RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.CENTER_REQUEST_MAPPING, page, size));
        return Converter.toCenterPageDTO(page);
    }


    @GetMapping
    @ApiOperation(value = "Get centers filtering by a term on fields: name, city, region, territorial, nature, owner and type", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)})
    public PageDTO<Center> findAll(@ApiParam(value = "Zero-based element index", required = true)
                                   @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                   @ApiParam(value = "The size of the page to be returned", required = true)
                                   @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                   @ApiParam(value = "The term to be found. Length must be greather or equal than 4")
                                   @RequestParam(value = RequestMappingConstants.TERM_PARAM, required = false) String term,
                                   UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        if (ObjectUtils.isEmpty(term))
            return buildResponse(uriBuilder, response, size, service.findAll(request));
        else if (term.length() >= MINIMAL_TERM_LENGTH)
            return buildResponse(uriBuilder, response, size, service.findByTerm(term, request));
        else
            throw new IllegalArgumentException("Term length must be greater than or equalTo " + MINIMAL_TERM_LENGTH);

    }

    @GetMapping(value = RequestMappingConstants.TYPE_REQUEST_MAPPING + RequestMappingConstants.TYPE_PATH_PARAM)
    @ApiOperation(value = "Get center list by type", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByType(@ApiParam(value = "The center type. Available values : 0, 1", required = true)
                                      @PathVariable(RequestMappingConstants.TYPE_PARAM) String type,
                                      @ApiParam(value = "Zero-based element index", required = true)
                                      @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                      @ApiParam(value = "The size of the page to be returned", required = true)
                                      @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                      UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByType(type, request));
    }


    @GetMapping(value = RequestMappingConstants.TYPE_REQUEST_MAPPING + RequestMappingConstants.TYPE_PATH_PARAM +
            RequestMappingConstants.CITY_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get center list by type and city", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByTypeAndCity(@ApiParam(value = "The center type. Available values : 0, 1", required = true)
                                             @PathVariable(RequestMappingConstants.TYPE_PARAM) String type,
                                             @ApiParam(value = "The city id for filter", required = true)
                                             @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                             @ApiParam(value = "Zero-based element index", required = true)
                                             @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                             @ApiParam(value = "The size of the page to be returned", required = true)
                                             @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                             UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByTypeAndCity(type, id, request));
    }

    @GetMapping(value = RequestMappingConstants.TYPE_REQUEST_MAPPING + RequestMappingConstants.TYPE_PATH_PARAM +
            RequestMappingConstants.REGION_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get center list by type and region", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByTypeAndRegion(@ApiParam(value = "The center type. Available values : 0, 1", required = true)
                                               @PathVariable(RequestMappingConstants.TYPE_PARAM) String type,
                                               @ApiParam(value = "The region id for filter", required = true)
                                               @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                               @ApiParam(value = "Zero-based element index", required = true)
                                               @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                               @ApiParam(value = "The size of the page to be returned", required = true)
                                               @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                               UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByTypeAndRegion(type, id, request));
    }

    @GetMapping(value = RequestMappingConstants.CITY_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get center list by city", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByCity(@ApiParam(value = "The city id for filter", required = true)
                                      @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                      @ApiParam(value = "Zero-based element index", required = true)
                                      @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                      @ApiParam(value = "The size of the page to be returned", required = true)
                                      @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                      UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByCity(id, request));
    }

    @GetMapping(value = RequestMappingConstants.REGION_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get center list by region", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByRegion(@ApiParam(value = "The region id for filter", required = true)
                                        @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                        @ApiParam(value = "Zero-based element index", required = true)
                                        @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                        @ApiParam(value = "The size of the page to be returned", required = true)
                                        @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                        UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByRegion(id, request));
    }

    @GetMapping(value = RequestMappingConstants.NAME_REQUEST_MAPPING + RequestMappingConstants.NAME_PATH_PARAM)
    @ApiOperation(value = "Get center list by name", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByName(@ApiParam(value = "The center name for filter with a minimal length of 5", required = true)
                                      @PathVariable(RequestMappingConstants.NAME_PARAM) String name,
                                      @ApiParam(value = "Zero-based element index", required = true)
                                      @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                      @ApiParam(value = "The size of the page to be returned", required = true)
                                      @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                      UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        if (name.length() < 5)
            throw new IllegalArgumentException("Name length too small for to search");
        return buildResponse(uriBuilder, response, size, service.findByNameContaining(name, request));
    }


    @GetMapping(value = RequestMappingConstants.LOCATION_REQUEST_MAPPING)
    @ApiOperation(value = "Get center list by location", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByLocation(@ApiParam(value = "Latitude of origin", required = true)
                                          @RequestParam(RequestMappingConstants.LATITUDE_PARAM) double latitude,
                                          @ApiParam(value = "Longitude of origin", required = true)
                                          @RequestParam(RequestMappingConstants.LONGITUDE_PARAM) double longitude,
                                          @ApiParam(value = "Distance value", required = true)
                                          @RequestParam(RequestMappingConstants.DISTANCE_PARAM) int distance,
                                          @ApiParam(value = "Zero-based element index", required = true)
                                          @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                          @ApiParam(value = "The size of the page to be returned", required = true)
                                          @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                          UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByLocation(latitude, longitude, distance, request));
    }

    @GetMapping(value = RequestMappingConstants.TERRITORIAL_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get centers filtering by territorial and a term on fields: name, city, region, nature, owner and type", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByTerritorial(@ApiParam(value = "The territorial id for filter", required = true)
                                             @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                             @ApiParam(value = "Zero-based element index", required = true)
                                             @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                             @ApiParam(value = "The size of the page to be returned", required = true)
                                             @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                             @ApiParam(value = "The term to be found. Length must be greather or equal than 4")
                                             @RequestParam(value = RequestMappingConstants.TERM_PARAM, required = false) String term,
                                             UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        if (ObjectUtils.isEmpty(term))
            return buildResponse(uriBuilder, response, size, service.findByTerritorial(id, request));
        else if (term.length() >= MINIMAL_TERM_LENGTH)
            return buildResponse(uriBuilder, response, size, service.findByTerritorialAndTerm(id, term, request));
        else
            throw new IllegalArgumentException("Term length must be greater than or equalTo " + MINIMAL_TERM_LENGTH);
    }

    @GetMapping(value = RequestMappingConstants.NATURE_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get center list by nature", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByNature(@ApiParam(value = "The nature id for filter", required = true)
                                        @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                        @ApiParam(value = "Zero-based element index", required = true)
                                        @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                        @ApiParam(value = "The size of the page to be returned", required = true)
                                        @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                        UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByNature(id, request));
    }

    @GetMapping(value = RequestMappingConstants.OWNER_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get center list by owner", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Center> findByOwner(@ApiParam(value = "The owner id for filter", required = true)
                                       @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                       @ApiParam(value = "Zero-based element index", required = true)
                                       @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                       @ApiParam(value = "The size of the page to be returned", required = true)
                                       @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                       UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size);
        return buildResponse(uriBuilder, response, size, service.findByOwner(id, request));
    }
}
