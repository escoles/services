package cat.jaumemoron.centers.services.controller.api.v1;

import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.persistence.dto.NumberDataDTO;
import cat.jaumemoron.centers.persistence.importer.GenericImporterResult;
import cat.jaumemoron.centers.persistence.service.AppointmentDataService;
import cat.jaumemoron.centers.persistence.service.AppointmentService;
import cat.jaumemoron.centers.persistence.service.SeasonsService;
import cat.jaumemoron.centers.persistence.service.TerritorialService;
import cat.jaumemoron.centers.persistence.utils.SecurityUtils;
import cat.jaumemoron.centers.services.component.AppointmentSchedulerComponent;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.dto.ErrorResponseDTO;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.exception.ResourceNotFoundException;
import cat.jaumemoron.centers.services.util.Converter;
import cat.jaumemoron.centers.services.util.PaginationUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING)
@Api(value = "Service to get data about appointments", tags = "Appointments")
public class AppointmentsController {

    @Autowired
    private AppointmentService service;

    @Autowired
    private AppointmentDataService dataService;

    @Autowired
    private AppointmentSchedulerComponent appointmentSchedulerComponent;

    @Autowired
    private SeasonsService seasonsService;

    @Autowired
    private TerritorialService territorialService;

    @GetMapping(value = RequestMappingConstants.TERRITORIAL_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get appointments of a territorial", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Appointment> findByTerritorial(@ApiParam(value = "The territorial id for filter", required = true)
                                                  @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                                  @ApiParam(value = "Zero-based element index", required = true)
                                                  @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                                  @ApiParam(value = "The size of the page to be returned", required = true)
                                                  @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                                  @ApiParam(value = "The season that appointments must belong")
                                                  @RequestParam(value = RequestMappingConstants.SEASON_PARAM) String season,
                                                  @ApiParam(value = "The speciality id for filter")
                                                  @RequestParam(value = RequestMappingConstants.SPECIALITY, required = false) String speciality,
                                                  UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size, Sort.by(Sort.Direction.DESC, "date", "number"));
        if (ObjectUtils.isEmpty(speciality))
            return buildResponse(uriBuilder, response, size, service.findByTerritorial(season, id, request));
        else
            return buildResponse(uriBuilder, response, size, service.findByTerritorialAndSpeciality(season, id, speciality, request));
    }

    private PageDTO<Appointment> buildResponse(UriComponentsBuilder uriBuilder, HttpServletResponse response,
                                               int size, Page<Appointment> page) {
        if (page.getNumberOfElements() > 0)
            response.addHeader("Link", PaginationUtils.getLinkHeader(uriBuilder,
                    RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.APPOINTMENT
                    , page, size));
        return Converter.toAppointmentPageDTO(page);
    }

    @GetMapping(value = RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Get appointment by id", response = Appointment.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The appointment data", response = Appointment.class),
            @ApiResponse(code = 404, message = "No appointment was found", response = ErrorResponseDTO.class)}
    )
    public Appointment get(@ApiParam(value = "The appointment id", required = true)
                           @PathVariable(RequestMappingConstants.ID_PARAM) String id) {
        if (ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("Appointment id cannot be null");
        Optional<Appointment> optional = service.findById(id);
        if (!optional.isPresent())
            throw new ResourceNotFoundException("Appointment with id [" + id + "] does not exists");
        return optional.get();
    }

    @GetMapping(value = RequestMappingConstants.TERRITORIAL_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM +
            RequestMappingConstants.SPECIALITY_REQUEST_MAPPING)
    @ApiOperation(value = "Get the appointment data of a territorial", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<AppointmentData> findByTerritorialSpeciality(@ApiParam(value = "The territorial id for filter", required = true)
                                                                @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                                                @ApiParam(value = "Zero-based element index", required = true)
                                                                @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                                                @ApiParam(value = "The size of the page to be returned", required = true)
                                                                @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                                                @ApiParam(value = "The season that appointments must belong")
                                                                @RequestParam(value = RequestMappingConstants.SEASON_PARAM) String season,
                                                                @ApiParam(value = "The speciality id for filter")
                                                                @RequestParam(value = RequestMappingConstants.SPECIALITY, required = false) String speciality) {
        PageRequest request = PaginationUtils.getPageRequest(start, size,
                Sort.by(Sort.Direction.ASC, "speciality.id")
                        .and(Sort.by(Sort.Direction.DESC, "data.maxNumber")
                                .and(Sort.by(Sort.Direction.DESC, "data.maxDate"))));
        if (ObjectUtils.isEmpty(speciality))
            return Converter.toAppointmentDataPageDTO(dataService.findBySeasonAndTerritorial(season, id, request));
        else {
            return Converter.toAppointmentDataPageDTO(dataService.findBySeasonAndTerritorialAndSpeciality(season, id, speciality, request));
        }
    }

    @GetMapping(value = RequestMappingConstants.TERRITORIAL_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM +
            RequestMappingConstants.DAYTYPE_REQUEST_MAPPING)
    @ApiOperation(value = "Get the appointment data of a territorial", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<AppointmentDataDayType> findByTerritorialSpecialityDayType(@ApiParam(value = "The territorial id for filter", required = true)
                                                                              @PathVariable(RequestMappingConstants.ID_PARAM) String id,
                                                                              @ApiParam(value = "Zero-based element index", required = true)
                                                                              @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                                                              @ApiParam(value = "The size of the page to be returned", required = true)
                                                                              @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                                                              @ApiParam(value = "The season that appointments must belong")
                                                                              @RequestParam(value = RequestMappingConstants.SEASON_PARAM) String season,
                                                                              @ApiParam(value = "The daywork of appointments for filter")
                                                                              @RequestParam(value = RequestMappingConstants.DAYTYPE_PARAM, required = false) DayType dayType,
                                                                              @ApiParam(value = "The speciality id for filter")
                                                                              @RequestParam(value = RequestMappingConstants.SPECIALITY, required = false) String speciality) {
        PageRequest request = PaginationUtils.getPageRequest(start, size,
                Sort.by(Sort.Direction.ASC, "speciality.id")
                        .and(Sort.by(Sort.Direction.ASC, "dayType")));
        if (dayType == null) {
            if (ObjectUtils.isEmpty(speciality))
                return Converter.toAppointmentDataDayTypePageDTO(dataService.findBySeasonAndTerritorialIdAndDayTypeAll(season, id, request));
            else {
                return Converter.toAppointmentDataDayTypePageDTO(dataService.findBySeasonAndTerritorialAndSpecialityAllDayType(season, id, speciality, request));
            }
        } else {
            if (ObjectUtils.isEmpty(speciality))
                return Converter.toAppointmentDataDayTypePageDTO(dataService.findBySeasonAndTerritorialIdAndDayType(season, id, dayType, request));
            else {
                return Converter.toAppointmentDataDayTypePageDTO(dataService.findBySeasonAndTerritorialAndSpecialityAndDayType(season, id, speciality, dayType, request));
            }
        }
    }

    @PostMapping(RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING)
    public String loadAppointments(@RequestParam("date") @DateTimeFormat(pattern = PersistenceConstants.DEFAULT_DATE_FORMAT) Date date,
                                   @RequestParam(value = "url") String url, Model model) {
        SecurityUtils.initializeSecurityContextAsAdmin();
        GenericImporterResult result = appointmentSchedulerComponent.getAppointments(date, url);
        model.addAttribute("result", result);
        return RequestMappingConstants.ADMIN +
                RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING +
                RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING;
    }

    @GetMapping(value = RequestMappingConstants.ANALYTICS_REQUEST_MAPPING + RequestMappingConstants.NUMBER_REQUEST_MAPPING)
    public List<NumberDataDTO> getAnalytics(@ApiParam(value = "The territorial id for filter", required = true)
                                            @RequestParam(RequestMappingConstants.TERRITORIAL) String territorialId,
                                            @ApiParam(value = "The speciality id for filter", required = true)
                                            @RequestParam(value = RequestMappingConstants.SPECIALITY) String specialityId,
                                            @ApiParam(value = "The appointment number to find", required = true)
                                            @RequestParam(RequestMappingConstants.NUMBER) long number) {
        List<NumberDataDTO> list = new ArrayList<>();
        List<Season> seasonList = seasonsService.findAll();
        for (Season season : seasonList) {
            list.add(service.getNumberData(season.getId(), territorialId, specialityId, number));
            if (list.size() == 4)
                break;
        }
        return list;
    }


    @GetMapping(value = RequestMappingConstants.SPECIALITY_REQUEST_MAPPING)
    @ApiOperation(value = "Get the appointment data of a territorial", response = PageDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The page requested", response = PageDTO.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<AppointmentData> findByTerritorialSpeciality(@ApiParam(value = "The season that appointments must belong")
                                                                @RequestParam(value = RequestMappingConstants.SEASON_PARAM) String season,
                                                                @ApiParam(value = "The speciality id for filter")
                                                                @RequestParam(value = RequestMappingConstants.SPECIALITY) String speciality) {
        PageRequest request = PaginationUtils.getPageRequest(0, 1,
                Sort.by(Sort.Direction.ASC, "speciality.id")
                        .and(Sort.by(Sort.Direction.DESC, "data.maxNumber")
                                .and(Sort.by(Sort.Direction.DESC, "data.maxDate"))));
        List<AppointmentData> data = new ArrayList<>();
        List<Territorial> territorialList = territorialService.findAll();
        for (Territorial territorial: territorialList){
            Page<AppointmentData> page = dataService.findBySeasonAndTerritorialAndSpeciality(season, territorial.getId(), speciality, request);
            if (!page.getContent().isEmpty())
                data.add(page.getContent().get(0));
        }
        return Converter.toAppointmentDataPageDTO(new PageImpl<>(data));
    }
}
