package cat.jaumemoron.centers.services.controller;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.event.EventPublisher;
import cat.jaumemoron.centers.persistence.importer.*;
import cat.jaumemoron.centers.services.component.AppointmentSchedulerComponent;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.telegram.TelegramCommandBot;
import cat.jaumemoron.centers.services.twitter.TwitterService;
import org.assertj.core.util.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Date;

@Controller
@RequestMapping(RequestMappingConstants.ADMIN_REQUEST_MAPPING)
public class AdminController extends EventPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private CenterFileImporter centerFileImporter;

    @Autowired
    private TypeFileImporter typeFileImporter;

    @Autowired
    private AppointmentFileImporter appointmentFileImporter;

    @Autowired
    private AppointmentSchedulerComponent appointmentSchedulerComponent;

    @Autowired
    private TelegramCommandBot telegramCommandBot;

    @Autowired
    private TwitterService twitterService;

    @Value("${ustec.file.repository}")
    private String repository;

    private GenericImporterResult loadFile(MultipartFile multipartFile, GenericImporter importer) throws IOException {
        if (multipartFile == null)
            throw new IllegalArgumentException("Multipart file list cannot be null");
        File file = null;
        try {
            file = new File(System.getProperty("java.io.tmpdir") + "/" +
                    multipartFile.getOriginalFilename() + "_" + System.currentTimeMillis());
            file.deleteOnExit();
            multipartFile.transferTo(file);
            return importer.importFile(file.getAbsolutePath());
        } finally {
            if (file != null)
                Files.delete(file);
        }
    }

    @GetMapping(RequestMappingConstants.IMPORT_CENTER_REQUEST_MAPPING)
    public String initImportCenters() {
        return RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_CENTER_REQUEST_MAPPING;
    }

    @PostMapping(RequestMappingConstants.IMPORT_CENTER_REQUEST_MAPPING)
    public String importCenters(@RequestParam("file") MultipartFile multipartFile, Model model) throws IOException {
        GenericImporterResult result = loadFile(multipartFile, centerFileImporter);
        model.addAttribute("result", result);
        return RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_CENTER_REQUEST_MAPPING;
    }

    @GetMapping(RequestMappingConstants.IMPORT_TYPES_REQUEST_MAPPING)
    public String initImportTypes() {
        return RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_TYPES_REQUEST_MAPPING;
    }

    @PostMapping(value = RequestMappingConstants.IMPORT_TYPES_REQUEST_MAPPING)
    public String importTypes(@RequestParam("file") MultipartFile multipartFile, Model model) throws IOException {
        GenericImporterResult result = loadFile(multipartFile, typeFileImporter);
        model.addAttribute("result", result);
        return RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_TYPES_REQUEST_MAPPING;
    }

    @GetMapping(RequestMappingConstants.IMPORT_APPOINTMENT_REQUEST_MAPPING)
    public String initImportAppointments() {
        return RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_APPOINTMENT_REQUEST_MAPPING;
    }

    @PostMapping(RequestMappingConstants.IMPORT_APPOINTMENT_REQUEST_MAPPING)
    public String importAppointments(@RequestParam("file") MultipartFile multipartFile, Model model) throws IOException {
        GenericImporterResult result = loadFile(multipartFile, appointmentFileImporter);
        model.addAttribute("result", result);
        return RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_APPOINTMENT_REQUEST_MAPPING;
    }


    @GetMapping(RequestMappingConstants.ACTION_REQUEST_MAPPING)
    public String action() {
        return RequestMappingConstants.ADMIN + RequestMappingConstants.ACTION_REQUEST_MAPPING;
    }

    @GetMapping(RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING + RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING)
    public String initLoadAppointments() {
        return RequestMappingConstants.ADMIN +
                RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING +
                RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING;
    }

    @PostMapping(RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING + RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING)
    public String loadAppointments(@RequestParam("date") @DateTimeFormat(pattern = PersistenceConstants.DEFAULT_DATE_FORMAT) Date date,
                                   @RequestParam(value = "url", required = false) String url,
                                   Model model) {
        LOGGER.debug("Loading appointments from date [{}]", date);
        GenericImporterResult result = appointmentSchedulerComponent.getAppointments(date, url);
        LOGGER.debug("Appointmens loadded sucessfully.");
        model.addAttribute("result", result);
        return RequestMappingConstants.ADMIN +
                RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING +
                RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING;
    }

    @GetMapping(RequestMappingConstants.FILE_REQUEST_MAPPING)
    public String getFiles(Model model) {
        File repositoryFile = Paths.get(repository).toFile();
        if (repositoryFile.exists()) {
            model.addAttribute("files", repositoryFile.list());
        }
        return RequestMappingConstants.ADMIN + RequestMappingConstants.FILE_REQUEST_MAPPING;
    }

    @GetMapping(RequestMappingConstants.FILE_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    public void getFile(@PathVariable(RequestMappingConstants.ID_PARAM) String fileName, HttpServletResponse response) throws IOException {
        File file = Paths.get(repository + "/" + fileName).toFile();
        if (!file.exists())
            throw new IllegalArgumentException("File [" + fileName + "] does not exists");
        response.setContentType("application/force-download");
        response.setHeader("Content-Disposition", "filename=\"" + fileName + "\"");
        response.setHeader("Content-Length", String.valueOf(file.length()));
        // for jquery.fileDownload pluging
        Cookie cookies = new Cookie("fileDownload", "true");
        cookies.setPath("/");
        response.addCookie(cookies);
        try (InputStream in = new FileInputStream(file)) {
            FileCopyUtils.copy(in, response.getOutputStream());
        }
    }

    @GetMapping(RequestMappingConstants.STATISTIC_REQUEST_MAPPING)
    public String statistics() {
        return RequestMappingConstants.ADMIN + RequestMappingConstants.STATISTIC_REQUEST_MAPPING;
    }

    @GetMapping(RequestMappingConstants.SOCIAL_REQUEST_MAPPING)
    public String getSocial() {
        return RequestMappingConstants.ADMIN + RequestMappingConstants.SOCIAL_REQUEST_MAPPING;
    }

    @PostMapping(RequestMappingConstants.SOCIAL_REQUEST_MAPPING)
    public String postSocial(@RequestParam(RequestMappingConstants.TWITTER) String twitterText,
                             @RequestParam(RequestMappingConstants.TELEGRAM) String telegramText,
                             Model model) {
        if (!ObjectUtils.isEmpty(twitterText))
            twitterService.updateStatus(twitterText);
        if (!ObjectUtils.isEmpty(telegramText))
            telegramCommandBot.sendMessageToGroup(telegramText);
        model.addAttribute("message", "Petició executada correctament");
        return RequestMappingConstants.ADMIN + RequestMappingConstants.SOCIAL_REQUEST_MAPPING;
    }


}

