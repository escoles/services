package cat.jaumemoron.centers.services.controller;

import cat.jaumemoron.centers.persistence.domain.Season;
import cat.jaumemoron.centers.persistence.domain.Territorial;
import cat.jaumemoron.centers.persistence.service.AppointmentService;
import cat.jaumemoron.centers.persistence.service.CenterService;
import cat.jaumemoron.centers.persistence.service.SeasonsService;
import cat.jaumemoron.centers.persistence.service.TerritorialService;
import cat.jaumemoron.centers.persistence.utils.PersistenceUtils;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.dto.SeasonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MenuController {

    @Autowired
    private TerritorialService territorialService;

    @Autowired
    private CenterService centerService;

    @Autowired
    private SeasonsService seasonsService;

    @Autowired
    private AppointmentService appointmentService;

    @Value("${google.analytics.active}")
    private boolean googleAnalyticsActive;

    @ModelAttribute("isGoogleAnalyticsActive")
    public boolean isGoogleAnalyticsActive() {
        return googleAnalyticsActive;
    }

    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "/")
    public String home(Model model) {
        List<SeasonDTO> seasonDTOList = new ArrayList();
        List<Season> list = seasonsService.findAll();
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        for (Season season : list) {
            seasonDTOList.add(SeasonDTO.of(season.getId(), decimalFormat.format(appointmentService.countBySeasonId(season.getId()))));
        }
        model.addAttribute("seasonList", seasonDTOList);
        return "home";
    }

    @GetMapping(value = "/centers")
    public String centerList() {
        return "centers";
    }

    @GetMapping(value = "/centers/distance")
    public String centerDistance() {
        return "distance";
    }

    @GetMapping(value = RequestMappingConstants.TERRITORIAL_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    public String territorial(@PathVariable(RequestMappingConstants.ID_PARAM) String id, Model model) {
        if (fillTerritorialData(id, model))
            return "territorial";
        else
            return "error";
    }

    @GetMapping(value = RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING +
            RequestMappingConstants.TERRITORIAL_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    public String appointment(@PathVariable(RequestMappingConstants.ID_PARAM) String id, Model model) {
        if (fillTerritorialData(id, model))
            return "appointment";
        else
            return "error";
    }

    private boolean fillTerritorialData(String id, Model model) {
        Territorial territorial = Territorial.of(id, "");
        int index = territorialService.findAll().indexOf(territorial);
        if (index >= 0) {
            DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
            territorial = territorialService.findAll().get(index);
            long territorialTotal = centerService.countByTerritorial(territorial.getId());
            long total = centerService.count();
            String percent = "";
            if (total > 0)
                percent = decimalFormat.format(territorialTotal * 100 / total) + "%";
            model.addAttribute("territorialId", territorial.getId());
            model.addAttribute("territorialName", territorial.getName());
            model.addAttribute("territorialPercent", percent);
            model.addAttribute("territorialTotal", decimalFormat.format(territorialTotal));
            model.addAttribute("total", decimalFormat.format(total));
            model.addAttribute("seasonList", seasonsService.findAll());
            model.addAttribute("currentSeason", PersistenceUtils.getCurrentSeason());
            return true;
        } else
            return false;
    }

    @GetMapping(value = "/faqs")
    public String faqs() {
        return "faqs";
    }

    @GetMapping(value = "/links")
    public String links() {
        return "links";
    }

    @GetMapping(value = "/appointments")
    public String appointments() {
        return "appointments";
    }

    @GetMapping(value = "/cookies")
    public String cookies() {
        return "cookies";
    }

    @GetMapping(value = RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING + RequestMappingConstants.ANALYTICS_REQUEST_MAPPING)
    public String analytics() {
        return "analytics";
    }

    @GetMapping(value = RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING + RequestMappingConstants.SPECIALITY_REQUEST_MAPPING)
    public String speciality(Model model) {
        model.addAttribute("seasonList", seasonsService.findAll());
        return "speciality";
    }

}
