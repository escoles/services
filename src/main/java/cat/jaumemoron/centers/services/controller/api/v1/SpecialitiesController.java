package cat.jaumemoron.centers.services.controller.api.v1;

import cat.jaumemoron.centers.persistence.domain.AppointmentSpeciality;
import cat.jaumemoron.centers.persistence.domain.Speciality;
import cat.jaumemoron.centers.persistence.service.SpecialitiesService;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.dto.SpecialityDTO;
import cat.jaumemoron.centers.services.util.Converter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.SPECIALITY_REQUEST_MAPPING)
@Api(value = "Service to get the appointment specialities", tags = "Specialities")
public class SpecialitiesController {

    @Autowired
    private SpecialitiesService service;

    @GetMapping
    @ApiOperation(value = "Get all appointment specialities", response = AppointmentSpeciality.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The appointment specialities list", response = AppointmentSpeciality.class, responseContainer = "List")}
    )
    public ResponseEntity<List<SpecialityDTO>> findAll() {
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS))
                .body(Converter.toSpecialityDTO(service.findAll()));
    }


}
