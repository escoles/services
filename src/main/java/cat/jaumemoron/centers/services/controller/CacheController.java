package cat.jaumemoron.centers.services.controller;

import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.CACHE_REQUEST_MAPPING)
public class CacheController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheController.class);

    @Autowired
    private CacheManager cacheManager;

    @GetMapping
    public Collection<String> get() {
        return cacheManager.getCacheNames();
    }

    @DeleteMapping(value = RequestMappingConstants.ID_PATH_PARAM)
    public void clear(@PathVariable(RequestMappingConstants.ID_PARAM) String id) {
        clearCache(id);
    }

    @DeleteMapping(value = RequestMappingConstants.ALL)
    public void all() {
        Collection<String> cacheList = get();
        for (String cacheName : cacheList)
            clearCache(cacheName);
    }

    private void clearCache(String name) {
        if (this.cacheManager.getCacheNames().contains(name)) {
            Cache cache = cacheManager.getCache(name);
            if (cache != null) {
                cache.clear();
                LOGGER.info("Clearing cache name [" + name + "]");
            }
        }
    }
}
