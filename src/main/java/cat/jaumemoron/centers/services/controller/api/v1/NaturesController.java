package cat.jaumemoron.centers.services.controller.api.v1;

import cat.jaumemoron.centers.persistence.domain.Nature;
import cat.jaumemoron.centers.persistence.service.NatureService;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.NATURE_REQUEST_MAPPING)
@Api(value = "Service to get data about natures", tags = "Natures")
public class NaturesController {

    @Autowired
    private NatureService service;

    @GetMapping
    @ApiOperation(value = "Get all natures", response = Nature.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The nature list", response = Nature.class, responseContainer = "List")}
    )
    public ResponseEntity<List<Nature>> findAll() {
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS))
                .body(service.findAll());
    }

}
