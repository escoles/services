package cat.jaumemoron.centers.services.controller.api.v1;

import cat.jaumemoron.centers.persistence.domain.Action;
import cat.jaumemoron.centers.persistence.service.ActionService;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.dto.ErrorResponseDTO;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.util.Converter;
import cat.jaumemoron.centers.services.util.PaginationUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.ACTION_REQUEST_MAPPING)
@Api(value = "Service to get data about system actions", tags = "Actions")
public class ActionsController {

    @Autowired
    private ActionService service;

    @GetMapping
    @ApiOperation(value = "Get all system actions", response = Action.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The actions list", response = Action.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public PageDTO<Action> findAll(@ApiParam(value = "Zero-based element index", required = true)
                                   @RequestParam(RequestMappingConstants.START_PARAM) int start,
                                   @ApiParam(value = "The size of the page to be returned", required = true)
                                   @RequestParam(RequestMappingConstants.SIZE_PARAM) int size,
                                   @ApiParam(value = "The speciality id for filter")
                                   @RequestParam(value = RequestMappingConstants.TERM_PARAM, required = false) String term,
                                   UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        PageRequest request = PaginationUtils.getPageRequest(start, size, Sort.by(Sort.Direction.DESC, "date"));
        if (ObjectUtils.isEmpty(term) || term.length() < 4)
            return buildResponse(uriBuilder, response, size, service.findAll(request));
        else
            return buildResponse(uriBuilder, response, size, service.findByTerm(term, request));
    }

    private PageDTO<Action> buildResponse(UriComponentsBuilder uriBuilder, HttpServletResponse response,
                                          int size, Page<Action> page) {
        if (page.getNumberOfElements() > 0)
            response.addHeader("Link", PaginationUtils.getLinkHeader(uriBuilder, RequestMappingConstants.ACTION_REQUEST_MAPPING, page, size));
        return Converter.toActionPageDTO(page);
    }

    @GetMapping(value = RequestMappingConstants.READ_REQUEST_MAPPING)
    @ApiOperation(value = "Get unread system actions", response = Long.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The number of unread actions", response = Action.class, responseContainer = "List")}
    )
    public long countUnRead() {
        return service.countUnRead();
    }

    @PostMapping(value = RequestMappingConstants.READ_REQUEST_MAPPING + RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Mark an action as read")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation sucessfull"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public void markAsRead(@PathVariable(RequestMappingConstants.ID_PARAM) String id) {
        service.markAsRead(id);
    }

    @DeleteMapping(RequestMappingConstants.ID_PATH_PARAM)
    @ApiOperation(value = "Delete an action")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation sucessfull"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = ErrorResponseDTO.class)}
    )
    public void delete(@PathVariable(RequestMappingConstants.ID_PARAM) String id) {
        service.delete(id);
    }
}
