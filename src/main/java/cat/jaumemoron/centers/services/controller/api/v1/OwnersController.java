package cat.jaumemoron.centers.services.controller.api.v1;

import cat.jaumemoron.centers.persistence.domain.Nature;
import cat.jaumemoron.centers.persistence.domain.Owner;
import cat.jaumemoron.centers.persistence.service.OwnerService;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(RequestMappingConstants.V1_API_REQUEST_MAPPING + RequestMappingConstants.OWNER_REQUEST_MAPPING)
@Api(value = "Service to get data about owners", tags = "Owner")
public class OwnersController {

    @Autowired
    private OwnerService service;

    @GetMapping
    @ApiOperation(value = "Get all owners", response = Nature.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The owner list", response = Owner.class, responseContainer = "List")}
    )
    public ResponseEntity<List<Owner>> findAll() {
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS))
                .body(service.findAll());
    }

}
