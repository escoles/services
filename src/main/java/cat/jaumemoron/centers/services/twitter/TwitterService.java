package cat.jaumemoron.centers.services.twitter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

@Service
public class TwitterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterService.class);

    @Value("${social.twitter.appId}")
    private String appId;

    @Value("${social.twitter.appSecret}")
    private String appSecret;

    @Value("${social.twitter.token}")
    private String token;

    @Value("${social.twitter.tokenSecret}")
    private String tokenSecret;

    private TwitterFactory twitterFactory;

    @PostConstruct
    public void configure() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(appId)
                .setOAuthConsumerSecret(appSecret)
                .setOAuthAccessToken(token)
                .setOAuthAccessTokenSecret(tokenSecret);
        twitterFactory = new TwitterFactory(cb.build());
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public void updateStatus(String text) {
        updateStatus(Collections.singletonList(text));
    }

    public void updateStatus(List<String> tweetList) {
        if (!tweetList.isEmpty()) {
            Twitter twitter = twitterFactory.getInstance();
            Status status = null;
            int i = 0;
            do {
                LOGGER.debug("Trying to update Twitter status with [{}] tweets", tweetList.size());
                String text = tweetList.get(i);
                try {
                    //in_reply_to_status_id
                    if (status == null)
                        status = twitter.updateStatus(text);
                    else {
                        StatusUpdate statusUpdate = new StatusUpdate(text);
                        statusUpdate.setInReplyToStatusId(status.getInReplyToStatusId());
                        status = twitter.updateStatus(statusUpdate);
                    }
                } catch (TwitterException e) {
                    LOGGER.error("Twitter status cannot be updated", e);
                }
            } while (++i < tweetList.size());

        }
    }

}
