package cat.jaumemoron.centers.services.dto;

import io.swagger.annotations.ApiModelProperty;

public class SpecialityDTO {

    @ApiModelProperty(value = "Speciality id")
    private String id;

    @ApiModelProperty(value = "Speciality name")
    private String name;

    public SpecialityDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
