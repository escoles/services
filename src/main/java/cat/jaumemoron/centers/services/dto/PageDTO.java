package cat.jaumemoron.centers.services.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "Page", description = "API response when a list is returned")
public class PageDTO<T> {

    @ApiModelProperty(value = "The number of total pages", required = true)
    private int totalPages;
    @ApiModelProperty(value = "The total amount of elements", required = true)
    private long totalElements;
    @ApiModelProperty(value = "The number of elements on this response", required = true)
    private long numberOfElements;
    @ApiModelProperty(value = "The list of results", required = true)
    private List<T> content;

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public long getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(long numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
