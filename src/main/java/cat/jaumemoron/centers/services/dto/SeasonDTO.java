package cat.jaumemoron.centers.services.dto;

import cat.jaumemoron.centers.persistence.domain.Season;

public class SeasonDTO {

    private String season;
    private String numberOfAppointments;

    public SeasonDTO() {
    }

    private SeasonDTO(String season, String numberOfAppointments) {
        this.season = season;
        this.numberOfAppointments = numberOfAppointments;
    }

    public String getSeason() {
        return season;
    }

    public String getNumberOfAppointments() {
        return numberOfAppointments;
    }

    public static SeasonDTO of(String season, String numberOfAppointments){
        return new SeasonDTO(season, numberOfAppointments);
    }
}
