package cat.jaumemoron.centers.services.util;

import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.dto.SpecialityDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static PageDTO<Center> toCenterPageDTO(Page<Center> page) {
        PageDTO<Center> pageDTO = new PageDTO<Center>();
        copyProperties(page, pageDTO);
        return pageDTO;
    }

    public static PageDTO<Action> toActionPageDTO(Page<Action> page) {
        PageDTO<Action> pageDTO = new PageDTO<Action>();
        copyProperties(page, pageDTO);
        return pageDTO;
    }

    public static PageDTO<Appointment> toAppointmentPageDTO(Page<Appointment> page) {
        PageDTO<Appointment> pageDTO = new PageDTO<Appointment>();
        copyProperties(page, pageDTO);
        return pageDTO;
    }

    public static PageDTO<AppointmentData> toAppointmentDataPageDTO(Page<AppointmentData> page) {
        PageDTO<AppointmentData> pageDTO = new PageDTO<AppointmentData>();
        copyProperties(page, pageDTO);
        return pageDTO;
    }

    public static PageDTO<AppointmentDataDayType> toAppointmentDataDayTypePageDTO(Page<AppointmentDataDayType> page) {
        PageDTO<AppointmentDataDayType> pageDTO = new PageDTO<AppointmentDataDayType>();
        copyProperties(page, pageDTO);
        return pageDTO;
    }

    private static void copyProperties(Page source, PageDTO target) {
        BeanUtils.copyProperties(source, target);
        target.setNumberOfElements(source.getNumberOfElements());
    }

    public static PageDTO<Statistic> toStatisticPageDTO(Page<Statistic> page) {
        PageDTO<Statistic> pageDTO = new PageDTO<Statistic>();
        copyProperties(page, pageDTO);
        return pageDTO;
    }

    public static List<SpecialityDTO> toSpecialityDTO(List<Speciality> source) {
        List<SpecialityDTO> target = new ArrayList<>();
        for (Speciality speciality: source){
            SpecialityDTO dto = new SpecialityDTO(speciality.getId(), speciality.getName() +
                    " (" + speciality.getId() + ")");
            target.add(dto);
        }
        return target;
    }
}
