package cat.jaumemoron.centers.services.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.util.UriComponentsBuilder;

public class PaginationUtils {

    public static PageRequest getPageRequest(int start, int size) {
        return PageRequest.of(start / size, size);
    }

    public static PageRequest getPageRequest(int start, int size, Sort sort) {
        return PageRequest.of(start / size, size, sort);
    }

    public static String getLinkHeader(UriComponentsBuilder uriBuilder, String context, Page page, int size) {

        uriBuilder.path(context);

        StringBuilder linkHeader = new StringBuilder();
        if (page.hasNext()) {
            String uriNextPage = constructNextPageUri(uriBuilder, page.getNumber(), size);
            linkHeader.append(createLinkHeader(uriNextPage, "next"));
        }
        if (page.hasPrevious()) {
            String uriPrevPage = constructPrevPageUri(uriBuilder, page.getNumber(), size);
            appendCommaIfNecessary(linkHeader);
            linkHeader.append(createLinkHeader(uriPrevPage, "prev"));
        }
        if (page.isFirst()) {
            String uriFirstPage = constructFirstPageUri(uriBuilder, size);
            appendCommaIfNecessary(linkHeader);
            linkHeader.append(createLinkHeader(uriFirstPage, "first"));
        }
        if (page.isLast()) {
            String uriLastPage = constructLastPageUri(uriBuilder, page.getTotalPages(), size);
            appendCommaIfNecessary(linkHeader);
            linkHeader.append(createLinkHeader(uriLastPage, "last"));
        }
        return linkHeader.toString();
    }

    private static String constructNextPageUri(final UriComponentsBuilder uriBuilder, final int page, final int size) {
        return uriBuilder.replaceQueryParam("page", page + 1).replaceQueryParam("size", size).build().encode().toUriString();
    }

    private static String constructPrevPageUri(final UriComponentsBuilder uriBuilder, final int page, final int size) {
        return uriBuilder.replaceQueryParam("page", page - 1).replaceQueryParam("size", size).build().encode().toUriString();
    }

    private static String constructFirstPageUri(final UriComponentsBuilder uriBuilder, final int size) {
        return uriBuilder.replaceQueryParam("page", 0).replaceQueryParam("size", size).build().encode().toUriString();
    }

    private static String constructLastPageUri(final UriComponentsBuilder uriBuilder, final int totalPages, final int size) {
        return uriBuilder.replaceQueryParam("page", totalPages).replaceQueryParam("size", size).build().encode().toUriString();
    }

    private static void appendCommaIfNecessary(final StringBuilder linkHeader) {
        if (linkHeader.length() > 0) {
            linkHeader.append(", ");
        }
    }

    private static String createLinkHeader(final String uri, final String rel) {
        return "<" + uri + ">; rel=\"" + rel + "\"";
    }
}
