package cat.jaumemoron.centers.services.component;

import cat.jaumemoron.centers.persistence.component.UstecComponent2;
import cat.jaumemoron.centers.persistence.event.EventPublisher;
import cat.jaumemoron.centers.persistence.event.ImporterEvent;
import cat.jaumemoron.centers.persistence.importer.AppointmentFileImporter;
import cat.jaumemoron.centers.persistence.importer.GenericImporterResult;
import cat.jaumemoron.centers.persistence.service.AppointmentDataService;
import cat.jaumemoron.centers.persistence.utils.SecurityUtils;
import cat.jaumemoron.centers.services.event.SocialEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class AppointmentSchedulerComponent extends EventPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentSchedulerComponent.class);

    private Map<LocalDate, Long> executions = new HashMap<>();

    @Autowired
    private UstecComponent2 ustecComponent;

    @Autowired
    private AppointmentFileImporter importer;

    @Autowired
    private AppointmentDataService appointmentDataService;

    @Value("${component.appointments.days-to-process}")
    private int daysToProcess;

    @Scheduled(initialDelayString = "${ustec.initialDelay}", fixedRateString = "${ustec.fixedRate}")
    public void getAppointments() {
        try {
            // Inicialitzem contexte de seguretat
            SecurityUtils.initializeSecurityContextAsAdmin();
            // Obtenim les dades del dia
            executeCurrentDay();
            // Obtenim les dades dels últims 7 dies
            executeLastDays();
        } finally {
            // Netegem el contexte de seguretat
            SecurityContextHolder.clearContext();
        }
    }

    private void executeCurrentDay() {
        LocalDate date = LocalDate.now();
        LOGGER.info("Number of executions: {} ", executions.size());
        LOGGER.info("Processing date [{}]...", date);
        GenericImporterResult result = execute(date, true);
        // Actualitzem les estadístiques si cal
        if (result != null && result.getRecordsNumber() > 0) {
            LOGGER.info("Loaded [{}] records", result.getRecordsNumber());
            appointmentDataService.update();
            // Enviem event per publicar-ho a les xarxes socials si cal
            if (executions.get(date) != result.getRecordsNumber()) {
                LOGGER.info("New data loaded. Publishing a SocialEvent");
                publish(new SocialEvent(result.getRecordsNumber() + ""));
            }
            // Actualitzem el número de registres
            executions.put(date, result.getRecordsNumber());
        }
    }

    private void executeLastDays() {
        LocalDate date = LocalDate.now().minusDays(daysToProcess);
        for (int i = 0; i < daysToProcess; i++) {
            LOGGER.info("Processing date [{}]...", date);
            execute(date, false);
            date = date.plusDays(1);
        }
        // Actualitzem de nou les estadístiques
        appointmentDataService.update();
    }

    private GenericImporterResult execute(LocalDate date, boolean first) {
        GenericImporterResult result = getAppointments(Timestamp.valueOf(date.atStartOfDay()), null, false);
        if (first) {
            // Comprobem si l'execució existeix i, sino, la definim
            if (!executions.containsKey(date)) {
                if (result == null)
                    executions.put(date, 0L);
                else
                    executions.put(date, result.getRecordsNumber());
            }
            // Si hem obtingut registres actualitzem la data d'execució
            if (result != null && result.getRecordsNumber() > 0) {
                // Publiquem l'event de càrrega de fitxer si el nombre de registres és diferent
                if (executions.get(date) != result.getRecordsNumber())
                    publish(new ImporterEvent(result));
            }
        }

        return result;
    }

    public GenericImporterResult getAppointments(Date date, String url) {
        return getAppointments(date, url, true);
    }

    public GenericImporterResult getAppointments(Date date, String url, boolean publihResult) {
        GenericImporterResult result = null;
        LOGGER.debug("Executing process to update appointments using date [{}]", date);
        try {
            File file = ustecComponent.getAppointments(date, url);
            if (file != null && file.exists()) {
                result = importer.importFile(file.getAbsolutePath(), publihResult);
            }
        } catch (Exception e) {
            LOGGER.error("Error getting appointments: " + e.getMessage());
        }
        LOGGER.debug("Process finished for date [{}]", date);

        return result;
    }
}
