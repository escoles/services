package cat.jaumemoron.centers.services.component;

import cat.jaumemoron.centers.persistence.dto.TerritorialCountDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.*;

@Component
public class SpringTemplateComponent {

    public static final String TELEGRAM_NEW_APPOINTMENTS_TEMPLATE = "telegram/newAppointments.html";
    public static final String TWITTER_NEW_APPOINTMENTS_TEMPLATE = "twitter/newAppointments.html";
    public static final String TWITTER_TERRITORIAL_DETAIL_TEMPLATE = "twitter/territorialDetail.html";

    @Autowired
    private SpringTemplateEngine springTemplateEngine;

    public String getText(String template, Map<String, Object> variables) {
        Context ctx = new Context(Locale.ENGLISH);
        ctx.setVariables(variables);
        return springTemplateEngine.process(template, ctx);
    }

    public String getTelegramText() {
        return getText(TELEGRAM_NEW_APPOINTMENTS_TEMPLATE, new HashMap<>());
    }

    public List<String> getTwitterText(List<TerritorialCountDTO> appointmentDataList){
        List<String> messages = new ArrayList<>();
        Map<String, Object> attributes = new HashMap<>();
        messages.add(getText(TWITTER_NEW_APPOINTMENTS_TEMPLATE, attributes));
        for (TerritorialCountDTO data: appointmentDataList){
            attributes = new HashMap<>();
            attributes.put("territorial", data.getTerritorial());
            attributes.put("number", data.getCount());
            messages.add(getText(TWITTER_TERRITORIAL_DETAIL_TEMPLATE, attributes));
        }
        return messages;
    }
}
