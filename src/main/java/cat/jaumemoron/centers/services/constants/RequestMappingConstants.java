package cat.jaumemoron.centers.services.constants;

public class RequestMappingConstants {

    public static final String START_PARAM = "start";
    public static final String SIZE_PARAM = "length";

    public static final String LATITUDE_PARAM = "latitude";
    public static final String LONGITUDE_PARAM = "longitude";
    public static final String DISTANCE_PARAM = "distance";

    public static final String CENTER = "center";
    public static final String TYPE = "type";
    public static final String CITY = "city";
    public static final String REGION = "region";
    public static final String NAME = "name";
    public static final String LOCATION = "location";
    public static final String TERRITORIAL = "territorial";
    public static final String NATURE = "nature";
    public static final String OWNER = "owner";
    public static final String TERM = "term";
    public static final String ADMIN = "admin";

    public static final String V1_API_REQUEST_MAPPING = "/api/v1";

    public static final String CENTER_REQUEST_MAPPING = "/" + CENTER;

    public static final String ID_PARAM = "id";
    public static final String ID_PATH_PARAM = "/{" + ID_PARAM + "}";

    public static final String TYPE_PARAM = TYPE;
    public static final String TYPE_REQUEST_MAPPING = "/" + TYPE;
    public static final String TYPE_PATH_PARAM = "/{" + TYPE_PARAM + "}";

    public static final String CITY_REQUEST_MAPPING = "/" + CITY;
    public static final String REGION_REQUEST_MAPPING = "/" + REGION;
    public static final String NAME_PARAM = NAME;
    public static final String NAME_REQUEST_MAPPING = "/" + NAME;
    public static final String NAME_PATH_PARAM = "/{" + NAME_PARAM + "}";

    public static final String LOCATION_REQUEST_MAPPING = "/" + LOCATION;
    public static final String TERRITORIAL_REQUEST_MAPPING = "/" + TERRITORIAL;
    public static final String NATURE_REQUEST_MAPPING = "/" + NATURE;
    public static final String OWNER_REQUEST_MAPPING = "/" + OWNER;

    public static final String TERM_PARAM = "term";
    public static final String SEASON_PARAM = "season";
    public static final String DAYTYPE_PARAM = "daytype";

    public static final String DAYTYPE_REQUEST_MAPPING = "/" + DAYTYPE_PARAM;

    public static final String ACTION = "action";
    public static final String ACTION_REQUEST_MAPPING = "/" + ACTION;

    public static final String READ = "read";
    public static final String READ_REQUEST_MAPPING = "/" + READ;

    public static final String SPECIALITY = "speciality";
    public static final String SPECIALITY_REQUEST_MAPPING = "/" + SPECIALITY;

    public static final String APPOINTMENT = "appointment";
    public static final String APPOINTMENT_REQUEST_MAPPING = "/" + APPOINTMENT;

    public static final String CACHE = "cache";
    public static final String CACHE_REQUEST_MAPPING = "/" + CACHE;
    public static final String ALL = "all";

    public static final String ADMIN_REQUEST_MAPPING = "/" + ADMIN;

    public static final String IMPORT_REQUEST_MAPPING = "/import";

    public static final String LOAD_APPOINTMENT = "load";
    public static final String LOAD_APPOINTMENT_REQUEST_MAPPING = "/" + LOAD_APPOINTMENT;

    public static final String IMPORT_CENTER_REQUEST_MAPPING = IMPORT_REQUEST_MAPPING + CENTER_REQUEST_MAPPING;
    public static final String IMPORT_TYPES_REQUEST_MAPPING = IMPORT_REQUEST_MAPPING + TYPE_REQUEST_MAPPING;
    public static final String IMPORT_TERRITORIAL_REQUEST_MAPPING = IMPORT_REQUEST_MAPPING + TERRITORIAL_REQUEST_MAPPING;
    public static final String IMPORT_SPECIALITY_REQUEST_MAPPING = IMPORT_REQUEST_MAPPING + SPECIALITY_REQUEST_MAPPING;

    public static final String IMPORT_APPOINTMENT_REQUEST_MAPPING = IMPORT_REQUEST_MAPPING + APPOINTMENT_REQUEST_MAPPING;

    public static final String SPECIALITY_PATH_PARAM = "/{" + SPECIALITY + "}";

    public static final String FILE = "file";
    public static final String FILE_REQUEST_MAPPING = "/" + FILE;

    public static final String STATISTIC = "statistic";
    public static final String STATISTIC_REQUEST_MAPPING = "/" + STATISTIC;

    public static final String SOCIAL = "social";
    public static final String SOCIAL_REQUEST_MAPPING = "/" + SOCIAL;

    public static final String TWITTER = "twitter";
    public static final String TELEGRAM = "telegram";

    public static final String NUMBER = "number";
    public static final String NUMBER_REQUEST_MAPPING = "/" + NUMBER;
    public static final String ANALYTICS = "analytics";
    public static final String ANALYTICS_REQUEST_MAPPING = "/" + ANALYTICS;

}
