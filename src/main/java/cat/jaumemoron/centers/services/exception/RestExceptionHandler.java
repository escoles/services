package cat.jaumemoron.centers.services.exception;

import cat.jaumemoron.centers.persistence.domain.Action;
import cat.jaumemoron.centers.persistence.event.ActionEvent;
import cat.jaumemoron.centers.persistence.event.EventPublisher;
import cat.jaumemoron.centers.services.dto.ErrorResponseDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

@ControllerAdvice
public class RestExceptionHandler extends EventPublisher {

    private ResponseEntity<ErrorResponseDTO> buildResponse(HttpStatus status, String message, Exception exception) {
        ErrorResponseDTO error = new ErrorResponseDTO(status, message, exception.getLocalizedMessage());
        return new ResponseEntity<>(error, new HttpHeaders(), error.getStatus());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponseDTO> handleIllegalArgumentException(IllegalArgumentException exception) {
        return buildResponse(HttpStatus.BAD_REQUEST, "Wrong parameter error", exception);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponseDTO> handleException(Exception exception) {
        // Com és excepció no controlada la desem a bdd
        saveException(exception);
        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error", exception);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponseDTO> handleResourceNotFoundException(ResourceNotFoundException exception) {
        return buildResponse(HttpStatus.NOT_FOUND, "Entity not found", exception);
    }

    @ExceptionHandler(MissingServletRequestPartException.class)
    public ResponseEntity<ErrorResponseDTO> handleMissingServletRequestPartException(MissingServletRequestPartException exception) {
        return buildResponse(HttpStatus.BAD_REQUEST, "Wrong parameter error", exception);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorResponseDTO> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
        String message = "Parameter [" + exception.getName() + "] contains an invalid value [" + exception.getValue() + "]";
        return buildResponse(HttpStatus.BAD_REQUEST, message, exception);
    }

    private void saveException(Exception exception) {
        Action action = Action.of(exception.getMessage());
        action.addComment(exception.getLocalizedMessage());
        publish(new ActionEvent(action));

    }

}
