package cat.jaumemoron.centers.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Bean
    public Docket apiVersion1() {
        return getDocket("1.0", "cat.jaumemoron.centers.services.controller.api.v1");
    }

    private Docket getDocket(String version, String basePackage) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(version)
                .select()
                // Indicamos los controladors que queremos publicar en el API
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .build()
                // Indicamos que no muestre los mensajes de respuesta por defecto
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo(version));
    }

    private ApiInfo apiInfo(String version) {
        List<VendorExtension> emptyList = Collections.emptyList();
        String termsOfServiceUrl = null;
        String license = null;
        String licenseUrl = null;
        return new ApiInfo(
                "Centers API Rest",
                "API Rest to get data about public education centers in Catalunya",
                version,
                termsOfServiceUrl,
                new Contact("Jaume Morón i Tarrasa", "http://www.jaumemoron.cat",
                        "jaume.moron@gmail.com"),
                license, licenseUrl, emptyList);
    }

}
