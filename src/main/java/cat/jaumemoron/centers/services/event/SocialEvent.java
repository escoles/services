package cat.jaumemoron.centers.services.event;

import cat.jaumemoron.centers.persistence.event.PersistenceEvent;

public class SocialEvent extends PersistenceEvent<String> {

    public SocialEvent(String text) {
        super(text);
    }

}
