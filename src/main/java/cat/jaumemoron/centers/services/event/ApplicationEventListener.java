package cat.jaumemoron.centers.services.event;

import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.persistence.service.ActionService;
import cat.jaumemoron.centers.persistence.service.AppointmentDataService;
import cat.jaumemoron.centers.persistence.service.AppointmentService;
import cat.jaumemoron.centers.persistence.utils.PersistenceUtils;
import cat.jaumemoron.centers.persistence.utils.SecurityUtils;
import cat.jaumemoron.centers.services.component.SpringTemplateComponent;
import cat.jaumemoron.centers.services.telegram.TelegramCommandBot;
import cat.jaumemoron.centers.services.twitter.TwitterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Component
public class ApplicationEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationEventListener.class);

    @Autowired
    private TwitterService twitterService;

    @Autowired
    private TelegramCommandBot bot;

    @Autowired
    private AppointmentDataService service;

    @Autowired
    private AppointmentService appointmentService;

    @Value("${social.twitter.enabled}")
    private boolean twitterEnabled;

    @Value("${social.telegram.enabled}")
    private boolean telegramEnabled;

    @Autowired
    private SpringTemplateComponent springTemplateComponent;

    @Autowired
    private ActionService actionService;

    public void setTwitterEnabled(boolean twitterEnabled) {
        this.twitterEnabled = twitterEnabled;
    }

    public void setTelegramEnabled(boolean telegramEnabled) {
        this.telegramEnabled = telegramEnabled;
    }

    @Async
    @EventListener
    public void handleSocialEvent(SocialEvent event) {
        LOGGER.info("Processing a new social event");
        if (canSendMessage(LocalTime.now())) {
            LOGGER.info("Trying to send messages to Telegram/Twitter");
            sendtoTelegram();
            sendtoTwitter();
        }else
            LOGGER.info("Messages disabled to be out of time");
    }

    private void sendtoTwitter(){
        try{
            if (twitterEnabled){
                twitterService.updateStatus(getTwitterText());
            }else
                LOGGER.info("Twitter event listener is disabled");
        }catch (Exception e){
            LOGGER.error("Error updating Twitter status", e);
            Action action = Action.of(e.getMessage());
            actionService.create(action);
        }
    }

    private void sendtoTelegram(){
        try{
            if (telegramEnabled) {
                bot.sendMessageToGroup(springTemplateComponent.getTelegramText());
            } else
                LOGGER.info("Telegram event listener is disabled");
        }catch (Exception e){
            LOGGER.error("Error sending message to Telegram", e);
            Action action = Action.of(e.getMessage());
            actionService.create(action);
        }
    }

    List<String> getTwitterText(){
        return springTemplateComponent.getTwitterText(appointmentService.getTerritorialsLastNumber(PersistenceUtils.getCurrentSeason()));
    }

    protected boolean canSendMessage(LocalTime source){
        LocalTime init = LocalTime.of (7, 59, 59);
        LocalTime end = LocalTime.of (20, 0, 0);
        return source.isAfter(init) && source.isBefore(end);
    }
}
