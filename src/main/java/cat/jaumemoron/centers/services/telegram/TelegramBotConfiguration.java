package cat.jaumemoron.centers.services.telegram;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.PostConstruct;

@Component
public class TelegramBotConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramBotConfiguration.class);

    @Autowired
    private TelegramCommandBot bot;

    @Value("${social.telegram.enabled}")
    private boolean enabled;

    @PostConstruct
    public void init() {
        if (enabled) {
            LOGGER.warn("Starting telegram Bot...");
            try {
                TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);
                bot.configure();
                api.registerBot(bot);
                LOGGER.info("Telegram Bot started successfully");
            } catch (TelegramApiException e) {
                LOGGER.error("Error starting Telegram Bot", e);
            }
        } else
            LOGGER.warn("Telegram bot is disabled");
    }
}
