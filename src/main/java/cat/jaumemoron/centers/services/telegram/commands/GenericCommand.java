package cat.jaumemoron.centers.services.telegram.commands;

import cat.jaumemoron.centers.services.telegram.TelegramCommandBot;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;

public abstract class GenericCommand extends BotCommand {

    private TelegramCommandBot parent;

    public GenericCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    public TelegramCommandBot getParent() {
        return parent;
    }

    public void setParent(TelegramCommandBot parent) {
        this.parent = parent;
    }

    public void sendMessage(Long chatId, String text) {
        parent.sendTextMessage(chatId.toString(), text);
    }
}
