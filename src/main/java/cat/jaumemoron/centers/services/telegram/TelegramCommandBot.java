package cat.jaumemoron.centers.services.telegram;

import cat.jaumemoron.centers.persistence.domain.Action;
import cat.jaumemoron.centers.persistence.domain.AppointmentData;
import cat.jaumemoron.centers.persistence.domain.Season;
import cat.jaumemoron.centers.persistence.domain.Territorial;
import cat.jaumemoron.centers.persistence.event.ActionEvent;
import cat.jaumemoron.centers.persistence.event.TelegramRequestEvent;
import cat.jaumemoron.centers.persistence.service.AppointmentDataService;
import cat.jaumemoron.centers.persistence.service.SeasonsService;
import cat.jaumemoron.centers.persistence.service.TerritorialService;
import cat.jaumemoron.centers.services.component.SpringTemplateComponent;
import cat.jaumemoron.centers.services.telegram.commands.StartCommand;
import cat.jaumemoron.centers.services.util.PaginationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class TelegramCommandBot extends TelegramLongPollingCommandBot {

    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramCommandBot.class);

    private static final int MAX_TELEGRAM_MESSAGE_LINES = 100;

    private static final String LINE_SEPARATOR = "\n";

    @Value("${social.telegram.token}")
    private String token;

    @Value("${social.telegram.username}")
    private String username;

    @Value("${social.telegram.groupname}")
    private String groupName;

    @Value("${social.telegram.enabled}")
    private boolean enabled;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private TerritorialService territorialService;

    @Autowired
    private AppointmentDataService appointmentDataService;

    @Autowired
    private SeasonsService seasonsService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private SpringTemplateComponent springTemplateComponent;

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    public TerritorialService getTerritorialService() {
        return territorialService;
    }

    public void configure() {
        register(new StartCommand(springTemplateComponent.getText("telegram/territorialsList.html", null)));
        registerDefaultAction((absSender, message) -> {
            String response = "<b>El text introduït és erroni.</b>\n\n" + getTerritorialsList() +
                    getTerritorialsList();
            SendMessage commandUnknownMessage = new SendMessage();
            commandUnknownMessage.setChatId(message.getChatId().toString());
            commandUnknownMessage.setParseMode(ParseMode.HTML);
            commandUnknownMessage.setText(response);
            publisher.publishEvent(new TelegramRequestEvent(false));
            try {
                absSender.execute(commandUnknownMessage);
            } catch (TelegramApiException e) {
                LOGGER.error("Error sending default message {}", commandUnknownMessage, e);
            }
        });
    }


    @Override
    public void processNonCommandUpdate(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            if (message.hasText()) {
                String[] parameters = message.getText().split(" ");
                sendTextMessage(update.getMessage().getChatId().toString(), getReponse(parameters));
            }
        }
    }

    private String getTerritorialText(String name, List<AppointmentData> list) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("territorialName", name);
        attributes.put("specialityData", list);
        return springTemplateComponent.getText("telegram/territorialData.html", attributes);
    }

    private String getTerritorialsList() {
        return springTemplateComponent.getText("telegram/territorialsList.html", null);
    }

    private void sendTextMessage(SendMessage message) {
        if (enabled) {
            try {
                execute(message);
                LOGGER.debug("Message [{}] has been sent", message);
            } catch (TelegramApiException e) {
                LOGGER.error("Error sending message to Telegram", e);
                Action action = Action.of(e.getMessage());
                action.addComment(e.getLocalizedMessage());
                applicationEventPublisher.publishEvent(new ActionEvent(action));
            }
        } else
            LOGGER.debug("Telegram bot is disabled");
    }

    public void sendTextMessage(String chatId, String text) {
        int occurrence = StringUtils.countOccurrencesOf(text, LINE_SEPARATOR);
        if (occurrence > MAX_TELEGRAM_MESSAGE_LINES) {
            String[] lines = text.split(LINE_SEPARATOR);
            int counter = 0;
            StringBuilder message = new StringBuilder();
            for (String line : lines) {
                if (counter < MAX_TELEGRAM_MESSAGE_LINES) {
                    message.append(line);
                    message.append(LINE_SEPARATOR);
                    counter++;
                } else {
                    sendTextMessage(chatId, message.toString());
                    counter = 0;
                    message = new StringBuilder();
                }
            }
            sendTextMessage(chatId, message.toString());
        } else {
            SendMessage message = new SendMessage();
            message.setChatId(chatId);
            message.setParseMode(ParseMode.HTML);
            message.setText(text);
            sendTextMessage(message);
        }
    }

    public void sendMessageToGroup(String text) {
        sendTextMessage(groupName, text);
    }

    public String getReponse(String[] parameters) {
        PageRequest request = PaginationUtils.getPageRequest(0, 200,
                Sort.by(Sort.Direction.ASC, "speciality.id")
                        .and(Sort.by(Sort.Direction.DESC, "data.maxNumber")
                                .and(Sort.by(Sort.Direction.DESC, "data.maxDate"))));
        Season season = seasonsService.findAll().get(0);
        Season lastSeason = seasonsService.findAll().get(1);
        StringBuilder response = new StringBuilder();
        boolean responseOk = true;
        if (parameters.length == 0) {
            response.append(getTerritorialsList());
        } else if (parameters.length == 1) {
            Optional<Territorial> optional = getTerritorialService().findByAppointmentId(parameters[0]);
            if (optional.isPresent()) {
                Page<AppointmentData> page = appointmentDataService.findBySeasonAndTerritorial(season.getId(), optional.get().getId(), request);
                // Si no ha trobat cap resultat, anirem a buscar el curs anterior
                if (page.getTotalElements() == 0)
                    page = appointmentDataService.findBySeasonAndTerritorial(lastSeason.getId(), optional.get().getId(), request);
                response.append(getTerritorialText(optional.get().getName(), page.getContent()));
            } else {
                response.append("<b>El codi de territorial no existeix.</b>\n\n").append(getTerritorialsList());
                responseOk = false;
            }
        } else if (parameters.length == 2) {
            Optional<Territorial> optional = getTerritorialService().findByAppointmentId(parameters[0]);
            if (optional.isPresent()) {
                Page<AppointmentData> page = appointmentDataService.findBySeasonAndTerritorialAndSpeciality
                        (season.getId(), optional.get().getId(), parameters[1].toUpperCase(), request);
                if (page.getTotalElements() == 0)
                    page = appointmentDataService.findBySeasonAndTerritorialAndSpeciality
                            (lastSeason.getId(), optional.get().getId(), parameters[1].toUpperCase(), request);

                if (page.getTotalElements() > 0) {
                    response.append(getTerritorialText(optional.get().getName(), page.getContent()));
                } else {
                    response.append("<b>No s'han trobat dades per a aquest codi d'especialitat</b>\n\n");
                    responseOk = false;
                }
            } else {
                response.append("<b>El codi de territorial no existeix.</b>\n\n");
                responseOk = false;
            }
        }
        if (response.toString().isEmpty())
            response.append(getTerritorialsList());
        // Llancem event indicant si la petició que ha arribat és correcte o no
        publisher.publishEvent(new TelegramRequestEvent(responseOk));
        return response.toString();
    }
}