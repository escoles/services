package cat.jaumemoron.centers.services.telegram.commands;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class StartCommand extends BotCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartCommand.class);

    private static final String COMMAND = "start";

    private String text;

    public StartCommand() {
        super(COMMAND, "With this command you can start the Bot");
    }

    public StartCommand(String text) {
        super(COMMAND, "With this command you can start the Bot");
        this.text = text;
    }


    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        String userName = user.getFirstName() + " " + user.getLastName();
        SendMessage msg = new SendMessage();
        msg.setChatId(chat.getId().toString());
        msg.setText("Hola " + userName);
        msg.setParseMode(ParseMode.HTML);
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setText(getTerritorialsList());
        answer.setParseMode(ParseMode.HTML);
        try {
            absSender.execute(msg);
            absSender.execute(answer);
        } catch (TelegramApiException e) {
            LOGGER.error("Error in start Telegram command", e);
        }
    }

    private String getTerritorialsList() {
        return text;
    }
}

