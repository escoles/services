$(document).ready(function () {

    $(function () {
        loadTelegramStatisticData("/api/v1/statistic/0", 7);
    });

    var currentDate = new Date();

    function loadTelegramStatisticData(url, days){
        $.ajax({
            url: url,
            type: "get",
            data: {
                initDate: getDateAsString(new Date(currentDate-(86400000 * days))),
                endDate: getDateAsString(currentDate),
                start: 0,
                length: 10
            },
            success: function(response) {
                var data = contentToArrayData(response.content);
                var lineData = {
                    labels: data.labels,
                    datasets: [
                        {
                            label: "Total",
                            backgroundColor: 'rgba(26,179,148,0.5)',
                            pointBorderColor: "#fff",
                            borderColor: "rgba(26,179,148,0.7)",
                            pointBackgroundColor: "rgba(26,179,148,1)",
                            data: data.totals
                        },{
                            label: "Oks",
                            backgroundColor: 'rgba(130, 130, 130, 0.5)',
                            pointBorderColor: "#fff",
                            data: data.oks
                        },{
                            label: "Errors",
                            backgroundColor: 'rgba(191, 191, 191, 0.5)',
                            pointBorderColor: "#fff",
                            data: data.errors
                        }
                    ]
                };

                var lineOptions = {
                    responsive: true
                };
                var ctx = document.getElementById("lineChart").getContext("2d");
                chart = new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});

                var pieData = {
                    labels: ["Oks","Errors"],
                    datasets: [{
                        data: [data.pieOks,data.pieErrors],
                        backgroundColor: ["#a3e1d4","#dedede"]
                    }]
                };

                var pieOptions = {
                    responsive: true
                };

                var ctx4 = document.getElementById("pieChart").getContext("2d");
                new Chart(ctx4, {type: 'doughnut', data: pieData, options:pieOptions});

            },
            error: function(xhr) {
                alert("Error: " + xhr);
                console.log(xhr)
            }
        });
    }

});



function contentToArrayData(content) {
    var labels = [];
    var oks = [];
    var totals = [];
    var errors = [];
    var pieOks = 0;
    var pieErrors = 0;
    for (i = 0; i < content.length; i++) {
        labels[i] = getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].date))));
        oks[i] = content[i].numberOfOks;
        errors[i] = content[i].numberOfErrors;
        totals[i] = oks[i] + errors[i];
        pieOks = pieOks + oks[i];
        pieErrors = pieErrors + errors[i];
    }
    var data = {
        "labels" : labels,
        "totals" : totals,
        "oks" : oks,
        "errors" : errors,
        "pieOks" : pieOks,
        "pieErrors" : pieErrors
    };
    return data;
}