function getDateAsString(date) {
    var day = date.getDate();
    if (day<10)
        day = "0" + day;
    var month = date.getMonth() + 1; //January is 0!
    if (month<10)
        month = "0" + month;
    var year = date.getFullYear();
    return day + "/" + month + "/" + year;
}

function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
}

function getDayType(daype) {
    if (daype === 'FULL')
        return "Completa (1)";
    else if (daype === 'TREE_QUARTER')
        return "Tres quarts (0,83)";
    else if (daype === 'TWO_THIRDS')
        return "Dos terços (0,66)";
    else if (daype === 'HALF')
        return "Mitja jornada (0,5)";
    else if (daype === 'A_THIRD')
        return "Un terç (0,33)";
    else
        return "-";
}