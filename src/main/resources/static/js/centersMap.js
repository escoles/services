var map;
var latitude;
var longitude;
var mapPositionButtonLadda;
var mapAddressButtonLadda;


var markers = [];

$(document).ready(function () {

    mapPositionButtonLadda = $('#mapPositionButton').ladda();
    mapAddressButtonLadda = $('#mapAddressButton').ladda();

    $('#mapPositionButton').on('click', function () {
        if (navigator.geolocation) {
            mapPositionButtonLadda.ladda('start');
            navigator.geolocation.getCurrentPosition(getCurrentPosition);
        } else {
            alert("Geolocation is not supported by this browser");
        }
    });

    $('#mapAddressButton').on('click', function () {
        var address = $("#mapAddress").val();
        if (address !== null && address !== "") {
            mapAddressButtonLadda.ladda('start');
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode(
                {'address': address,
                'region': 'CA'}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    latitude = results[0].geometry.location.lat();
                    longitude = results[0].geometry.location.lng();
                    showMap();
                }
            });
        }
    });

    $('#mapDistance').change(function () {
        showMap()
    });


    $.get("/api/v1/type", function (data, status) {
        if (status === "success") {
            $('#typeSelect').append($('<option>', {value: ""}).text(""));
            $.each(data, function (i2, type) {
                $('#typeSelect').append($('<option>', {value: type.id}).text(type.name));
            });
        }
    });

    $('#natureCheck0').on('click', function () {
        evaluateIfShowMarkers();
    });
    $('#natureCheck1').on('click', function () {
        evaluateIfShowMarkers();
    });
    $('#natureCheck2').on('click', function () {
        evaluateIfShowMarkers();
    });
    $('#typeSelect').change(function () {
        evaluateIfShowMarkers();
    });

});

function getCurrentPosition(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    showMap();
}

function evaluateIfShowMarkers() {
    $.each(markers, function (i, marker) {
        evaluateIfShowMarker(marker);
    });
}

function evaluateIfShowMarker(marker) {
    var showNature = false;
    var showType = false;
    // Evaluem la titularitat
    if ($('#natureCheck0').prop("checked") === true)
        showNature = true;
    else if ($('#natureCheck1').prop("checked") === true && marker.center.nature.id === "1")
        showNature = true;
    else if ($('#natureCheck2').prop("checked") === true && marker.center.nature.id === "2")
        showNature = true;
    else
        showNature = false;
    // Evaluem el tipus d'ensenyament
    var typeSelected = $("#typeSelect").val();
    if (typeSelected !== "") {
        $.each(marker.center.typeList, function (i2, type) {
            if (type.id === typeSelected)
                showType = true;
        });
    } else
        showType = true;
    if (showNature && showType)
        marker.setMap(map);
    else
        marker.setMap(null);

}

function showMap() {
    var mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(latitude, longitude)
    };

    // Get map element
    var mapElement = document.getElementById('map');
    // Create the Google Map using elements
    map = new google.maps.Map(mapElement, mapOptions);
    // Obtenim la distància escollida
    var distance = $("#mapDistance").val();

    var url = "/api/v1/center/location?latitude=" + latitude + "&longitude=" + longitude + "&distance=" + distance + "&start=0&length=6000";

    $.get(url, function (data, status) {
        if (status === "success") {
            markers = [];
            var bounds = new google.maps.LatLngBounds ();
            var origin = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                map: map,
                icon: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
                title: "Origen"
            });

            $.each(data.content, function (i2, center) {
                // Icona per a escoles públiques
                var icon = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
                // Icona per a escoles privades
                if (center.nature.id === "2")
                    icon = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(center.location.lat, center.location.lon),
                    icon: icon,
                    title: center.name,
                    center: center
                });
                bounds.extend (new google.maps.LatLng(center.location.lat, center.location.lon));
                marker.addListener('click', function () {
                    $("#centerDataModalTitle").text(center.name);
                    $("#latitude").val(center.location.lat);
                    $("#longitude").val(center.location.lon);
                    $("#address").val(center.address);
                    $("#postalCode").val(center.postalCode);
                    $("#city").val(center.city.name);
                    $("#region").val(center.region.name);
                    $("#typeList").empty();
                    $.each(center.typeList, function (i, v) {
                            $("#typeList").append("<li>" + v.name + "</li>");
                        }
                    );
                    $("#phoneList").empty();
                    $.each(center.phoneList, function (i, v) {
                            $("#phoneList").append("<li>" + v + "</li>");
                        }
                    );
                    $("#emailList").empty();
                    $.each(center.emailList, function (i, v) {
                            $("#emailList").append("<li>" + v + "</li>");
                        }
                    );
                    var fax = $("#faxList");
                    fax.empty();
                    fax.append("<li>" + center.fax + "</li>");
                    $("#territorial").val(center.territorial.name);
                    $("#nature").val(center.nature.name);
                    $("#owner").val(center.owner.name);
                    // Mostrem el modal amb les dades
                    $('#centerDataModal').modal('show');
                });
                evaluateIfShowMarker(marker);
                markers.push(marker);
                bounds.extend (new google.maps.LatLng(center.location.lat, center.location.lon));
            });
            if (markers.length>1)
                map.fitBounds (bounds);
        }
    });

    // Show tab
    document.getElementById("mapTabContent").style.display = "block";
    mapPositionButtonLadda.ladda('stop');
    mapAddressButtonLadda.ladda('stop');
}