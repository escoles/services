Date.daysBetween = function (date1, date2) {   //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;        // Convert back to days and return
    return Math.round(difference_ms / one_day);
};

Date.getDateFromJson = function (value) {
    return new Date(JSON.parse(JSON.stringify(value)))
};

var dataTableAppointmentNumber = null;

$(document).ready(function () {
    $.get("/api/v1/speciality", function (data, status) {
        if (status === 'success') {
            $("#specialityTypeHead").typeahead({source: data});
            if ($("#analyticsSpecialityTypeHead"))
                $("#analyticsSpecialityTypeHead").typeahead({source: data});

        }
    });

    $.get("/api/v1/territorial", function (data, status) {
        if (status === 'success') {
            $('#territorial').append($('<option>', {value: ""}).text(""));
            $.each(data, function (i2, territorial) {
                $('#territorial').append($('<option>', {value: territorial.id}).text(territorial.name));
            });
        }
    });

});

$(document).on("change", "#analyticsSpecialityTypeHead", function (e) {
    // Validar valor
    var current = $(this).typeahead("getActive");
    if ($(this).val().length === 0)
        $("#speciality").val('');
    else
        $("#speciality").val(current.id);
});

$(document).on("click", "#doAnalytics", function (e) {
    var div = document.getElementById("resultsDiv");
    if ($("#territorial").val() !== '' && $("#speciality").val() !== '' && $("#number").val() !== '') {
        div.style.display = "none";
        // TODO: Validar combos
        var url = "/api/v1/appointment/analytics/number?number=" + $("#number").val() + "&speciality="
            + $("#speciality").val() + "&territorial=" + $("#territorial").val();
        $.get(url, function (data, status) {
            if (status === 'success') {
                div.style.display = "block";
                setYearData(data, 0);
                setYearData(data, 1);
                setYearData(data, 2);
                setYearData(data, 3);
            }
        });
    }
});


$(document).on("click", "#resetAnayliticsSpeciality", function (e) {
    $("#analyticsSpecialityTypeHead").val('');
    $("#speciality").val('');
});

function setYearData(data, year) {
    document.getElementById('any' + year).innerHTML = data[year].season;
    if (data[year].appointmentList.length > 0) {
        document.getElementById('label0' + year).innerHTML = formatNumber(data[year].appointmentList[0].number);
        if (JSON.parse(data[year].found === false))
            document.getElementById('label1' + year).innerHTML = '<small> Número no trobat. Mostrem el número més proper</small>';
        else
            document.getElementById('label1' + year).innerHTML = '';
        var date = new Date();
        var dateString = getDateAsString(new Date(JSON.parse(JSON.stringify(data[year].appointmentList[0].date))));
        document.getElementById('label2' + year).innerHTML = 'Nomenat per primer cop el ' + dateString;
        document.getElementById('label3' + year).innerHTML = 'Jornada: ' + getDayType(data[year].appointmentList[0].dayType);
        if (data[year].appointmentList.length > 1)
            document.getElementById('label4' + year).innerHTML = "S'ha nomenat " + data[year].appointmentList.length + " vegades més. "
                + "<a href='javascript:showAppointmentsModal(" + year + "," + data[year].appointmentList[0].number + ")'>Veure-les</a>";
    } else {
        document.getElementById('label0' + year).innerHTML = "";
        document.getElementById('label1' + year).innerHTML = "No s'ha trobat informació";
        document.getElementById('label2' + year).innerHTML = "";
        document.getElementById('label3' + year).innerHTML = "";
        document.getElementById('label4' + year).innerHTML = "";
    }
}


function showAppointmentsModal(year, number) {

    if (dataTableAppointmentNumber === null) {
        dataTableAppointmentNumber = $('#appointments-number-datatable').DataTable({
            iDisplayLength: 5,
            pageLength: 5,
            responsive: true,
            ordering: false,
            searching: false,
            paging: true,
            info: true,
            lengthMenu: [5, 10],
            language: {
                url: '/localization/es_CA.json'
            },
            columns: [
                {"data": "date"},
                {"data": "dayType"},
                {"data": "init"},
                {"data": "end"},
                {"data": "time"},
                {"data": "centerName"}
            ]
        });
    }

    var url = "/api/v1/appointment/analytics/number?number=" + number + "&speciality="
        + $("#speciality").val() + "&territorial=" + $("#territorial").val();
    $.get(url, function (data, status) {
        if (status === 'success') {
            var rows = [];
            document.getElementById('modalTitle').innerHTML = "Nomenaments del número " + formatNumber(number)
                + " de l'any " + data[year].appointmentList[0].season.id;
            for (i = 0; i < data[year].appointmentList.length; i++) {
                var time = Date.daysBetween(Date.getDateFromJson(data[year].appointmentList[i].init),
                    Date.getDateFromJson(data[year].appointmentList[i].end));
                if (time > 0)
                    time = time + ' dies';
                else
                    time = '-';
                rows[i] = {
                    "date": getDateAsString(new Date(JSON.parse(JSON.stringify(data[year].appointmentList[i].date)))),
                    "dayType": getDayType(data[year].appointmentList[i].dayType),
                    "centerName": data[year].appointmentList[i].centerName,
                    "init": getDateAsString(new Date(JSON.parse(JSON.stringify(data[year].appointmentList[i].init)))),
                    "end": getDateAsString(new Date(JSON.parse(JSON.stringify(data[year].appointmentList[i].end)))),
                    "time": time
                };
            }
            dataTableAppointmentNumber.clear();
            dataTableAppointmentNumber.rows.add(rows).draw();
            $('#appointmentsDataModal').modal('show');
        }
    });

}