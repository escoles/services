var userPositionButtonLadda;
var userAddressButtonLadda;

var centerExportOptions = {
    exportOptions: {
        format: {
            body: function (data, column, row, node) {
                if (column === 2) {
                    var types = "";
                    for (i = 0; i < data.length; i++) {
                        types = types + data[i].name + ",";
                    }
                    return types;
                }
                if (column === 3)
                    return data.name;
                else
                    return data;
            }
        }
    }
};

$(document).ready(function () {

    var url = $('#url').val();
    var dataTable = $('#centers-datatable').DataTable({
        serverSide: true,
        processing: true,
        pageLength: 25,
        responsive: true,
        ordering: false,
        searching: false,
        dom: 'Tflp<"html5buttons"B><"bottom"gitp>i',
        lengthMenu: [10, 25, 50],
        language: {
            url: '/localization/es_CA.json'
        },
        buttons: [
            $.extend(true, {}, centerExportOptions, {
                extend: 'copy'
            }),
            $.extend(true, {}, centerExportOptions, {
                extend: 'excel'
            }),
            $.extend(true, {}, centerExportOptions, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            })
        ],
        ajax: {
            contentType: 'application/json',
            url: url,
            type: 'GET',
            data: function (d) {
                d.term = $("#term").val()
            },
            dataFilter: function (data) {
                var json = {};
                var jsonData = jQuery.parseJSON(data);
                json.recordsTotal = jsonData.totalElements;
                json.recordsFiltered = jsonData.totalElements;
                json.data = contentToCenterData(jsonData.content); // data.result
                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {"data": "name"},
            {"data": "fullAddress"},
            {"data": "types"},
            {"data": "natureName"}
        ]
    });

    $(document).on("change keyup paste", "#term", function (e) {
        if ($(this).val().length == 0 || $(this).val().length >= 4) {
            dataTable.ajax.reload();
        }
    });

    $('#centers-datatable tbody').on('dblclick', 'tr', function () {
        var data = dataTable.row(this).data();
        $("#centerDataModalTitle").text(data["name"]);
        $("#latitude").val(data["location"].lat);
        $("#longitude").val(data["location"].lon);
        $("#address").val(data["address"]);
        $("#postalCode").val(data["postalCode"]);
        $("#city").val(data["city"].name);
        $("#region").val(data["region"].name);
        $("#typeList").empty();
        $.each(
            data["typeList"], function (i, v) {
                $("#typeList").append("<li>" + v.name + "</li>");
            }
        );
        $("#phoneList").empty();
        $.each(
            data["phoneList"], function (i, v) {
                $("#phoneList").append("<li>" + v + "</li>");
            }
        );
        $("#emailList").empty();
        $.each(
            data["emailList"], function (i, v) {
                $("#emailList").append("<li>" + v + "</li>");
            }
        );
        var fax = $("#faxList");
        fax.empty();
        fax.append("<li>" + data["fax"] + "</li>");
        $("#territorial").val(data["territorial"].name);
        $("#nature").val(data["nature"].name);
        $("#owner").val(data["owner"].name);
        // Mostrem el modal amb les dades
        $('#centerDataModal').modal('show');
    });

    userPositionButtonLadda = $('#userPositionButton').ladda();
    userAddressButtonLadda = $('#userAddressButton').ladda();

    $('#userPositionButton').on('click', function () {
        if (navigator.geolocation) {
            userPositionButtonLadda.ladda('start');
            navigator.geolocation.getCurrentPosition(getPosition);
        } else {
            alert("Geolocation is not supported by this browser");
        }
    });

    $('#userAddressButton').on('click', function () {
        var origin = $("#userAddress").val();
        if (origin !== null && origin !== "") {
            userAddressButtonLadda.ladda('start');
            calculeRoute(origin);
        }
    });

    // Quan desapareix el modal el resetegem
    $('#centerDataModal').on('hidden.bs.modal', function () {
        for (i = 0; i < 1; i++) {
            // Mostrem els textos
            document.getElementById("route" + i + "originLabel").style.display = "none";
            document.getElementById("route" + i + "destinationLabel").style.display = "none";
            document.getElementById("route" + i + "distanceLabel").style.display = "none";
            document.getElementById("route" + i + "durationLabel").style.display = "none";
            document.getElementById("route" + i + "stepsLabel").style.display = "none";
            $("#route" + i + "origin").text("");
            $("#route" + i + "destination").text("");
            $("#route" + i + "distance").text("");
            $("#route" + i + "duration").text("");
            $("#route" + i + "List").empty();
            $("#userAddress").val("");
        }
    });
});

function getPosition(position) {
    calculeRoute(position.coords.latitude + "," + position.coords.longitude);
}


function calculeRoute(origin) {
    var destination = $("#address").val() + ", " + $("#postalCode").val() + ", " + $("#city").val();
    // Calculem les diferents rutes
    calculeSingleRoute(origin, destination, google.maps.DirectionsTravelMode.WALKING, 3);
    calculeSingleRoute(origin, destination, google.maps.DirectionsTravelMode.BICYCLING, 2);
    calculeSingleRoute(origin, destination, google.maps.DirectionsTravelMode.TRANSIT, 1);
    calculeSingleRoute(origin, destination, google.maps.DirectionsTravelMode.DRIVING, 0);
    // Mostrem tot el contingut
    document.getElementById("routeTabContent").style.display = "block";
    userPositionButtonLadda.ladda('stop');
    userAddressButtonLadda.ladda('stop');
}

function calculeSingleRoute(origin, destination, travelMode, indexTab) {
    var directionsService = new google.maps.DirectionsService;
    var request = {
        origin: origin,
        destination: destination,
        travelMode: travelMode,
        region: 'es'
    };
    directionsService.route(request, function (response, status) {
        if (status === 'OK') {
            $.each(response.routes, function (i, route) {
                    // Obtenim les dades
                    var distance = route.legs[0].distance.text;
                    var duration = route.legs[0].duration.text;
                    // Mostrem els textos
                    document.getElementById("route" + indexTab + "originLabel").style.display = "block";
                    document.getElementById("route" + indexTab + "destinationLabel").style.display = "block";
                    document.getElementById("route" + indexTab + "distanceLabel").style.display = "block";
                    document.getElementById("route" + indexTab + "durationLabel").style.display = "block";
                    document.getElementById("route" + indexTab + "stepsLabel").style.display = "block";
                    // Mostrem els valors
                    $("#route" + indexTab + "origin").text(route.legs[0].start_address);
                    $("#route" + indexTab + "destination").text(route.legs[0].end_address);
                    $("#route" + indexTab + "distance").text(distance);
                    $("#route" + indexTab + "duration").text(duration);
                    $("#route" + indexTab + "List").empty();
                    // Mostrem els passos a seguir
                    $.each(route.legs[0].steps, function (i2, step) {
                            var text = step.instructions;
                            if (step.travel_mode === 'TRANSIT') {
                                var style = "style='color:" + step.transit.line.text_color + ";background-color:" + step.transit.line.color + "'";
                                var href = "";
                                if (step.transit.line.url !== undefined)
                                    href = step.transit.line.url;
                                else if (step.transit.line.agencies.length > 0 && step.transit.line.agencies[0].url !== '') {
                                    href = step.transit.line.agencies[0].url;
                                }
                                if (href !== "")
                                    href = "target='_new' href='" + href + "'";
                                var name = "<a " + href + " " + style + " data-toggle='tooltip' title=\""
                                    + step.transit.line.name + "\">" + step.transit.line.short_name + "</a>";
                                var parades = "";
                                if (step.transit.num_stops !== '') {
                                    parades = ", " + step.transit.num_stops + " parades"
                                }
                                text = text + " (" + name + parades + ")";
                            }
                            text = text + ": " + step.distance.text + ", " + step.duration.text + "";
                            $("#route" + indexTab + "List").append("<li>" + text + "</li>");
                        }
                    );
                }
            );
        }
    });
}

function contentToCenterData(content) {
    var data = [];
    for (i = 0; i < content.length; i++) {
        var fullAddress = content[i].address + ", " + content[i].postalCode + ", " + content[i].city.name + ", " + content[i].region.name;
        var types = "";
        for (j = 0; j < content[i].typeList.length; j++) {
            types = types + content[i].typeList[j].name + "<br>"
        }
        data[i] = {
            "id": content[i].id,
            "typeList": content[i].typeList,
            "name": content[i].name,
            "address": content[i].address,
            "postalCode": content[i].postalCode,
            "city": content[i].city,
            "region": content[i].region,
            "phoneList": content[i].phoneList,
            "emailList": content[i].emailList,
            "fax": content[i].fax,
            "territorial": content[i].territorial,
            "nature": content[i].nature,
            "owner": content[i].owner,
            "location": content[i].location,
            "fullAddress": fullAddress,
            "natureName": content[i].nature.name,
            "types": types
        };
    }
    return data;
}