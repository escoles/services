var barchart = null;
$(document).ready(function () {
    var urlSpeciality = $('#urlSpeciality').val();
    var dataTableSpeciality = null;

    $(document).on("change", "#specialityTypeHead", function (e) {
        var current = $(this).typeahead("getActive");
        if ($(this).val().length === 0)
            $("#speciality").val('');
        else
            $("#speciality").val(current.id);
    });

    $(document).on("click", "#findSpeciality", function (e) {
        if ($("#speciality").val() !== '' && dataTableSpeciality == null) {

            dataTableSpeciality = $('#speciality-datatable').DataTable({
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ordering: false,
                searching: false,
                paging: false,
                bInfo: false,
                dom: 'Tlp<"html5buttons"B><"bottom"gitp>i',
                lengthMenu: [10, 25, 50],
                language: {
                    url: '/localization/es_CA.json'
                },
                buttons: [
                    {extend: 'excel'},
                    {extend: 'copy'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                ajax: {
                    contentType: 'application/json',
                    url: urlSpeciality,
                    type: 'GET',
                    data: function (d) {
                        d.speciality = $("#speciality").val();
                        d.season = $("#season").val();
                    },
                    dataFilter: function (data) {
                        var json = {};
                        var jsonData = jQuery.parseJSON(data);
                        // Obtenim les dades per pintar el gràfic
                        var barData = {
                            labels: getBarChartLabel(jsonData.content),
                            datasets: [
                                {
                                    label: "Nomenaments",
                                    backgroundColor: 'rgba(24, 166, 137,1)',
                                    borderColor: "rgba(24, 166, 137,0.7)",
                                    pointBackgroundColor: "rgba(24, 166, 137,1)",
                                    pointBorderColor: "#fff",
                                    data: getBarChartData(jsonData.content)
                                }
                            ]
                        };
                        if (barchart != null) {
                            barchart.destroy();
                        }
                        var barOptions = {
                            responsive: true
                        };
                        var ctx2 = document.getElementById("barChart").getContext("2d");
                        var params = {type: 'bar', data: barData, options: barOptions};
                        barchart = new Chart(ctx2, params);
                        // Preparem les dades per ser compatibles amb el Datatable
                        json.recordsTotal = jsonData.length;
                        json.recordsFiltered = jsonData.length;
                        json.data = contentToSpecialityData(jsonData.content); // data.result
                        return JSON.stringify(json); // return JSON string
                    }
                },
                columns: [
                    {"data": "territorial"},
                    {"data": "maxNumber"},
                    {"data": "lastNumber"},
                    {"data": "total"}
                ]
            });
            document.getElementById("specialityDataRow").style.display = "block";
        }
        dataTableSpeciality.ajax.reload();
    });

    $.get("/api/v1/speciality", function (data, status) {
        if (status === 'success') {
            $("#specialityTypeHead").typeahead({source: data});
        }
    });

});


function contentToSpecialityData(content) {
    var data = [];
    for (i = 0; i < content.length; i++) {
        var territorialId = "";
        var territorialName = "";
        if (content[i].speciality) {
            territorialId = content[i].territorial.id;
            territorialName = content[i].territorial.name;
        }
        data[i] = {
            "territorial": territorialName,
            "maxNumber": formatNumber(content[i].data.maxNumber) + " ("
                + getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].data.maxDate)))) + ")",
            "lastNumber": formatNumber(content[i].data.lastNumber) + " ("
                + getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].data.lastDate)))) + ")",
            "total": formatNumber(content[i].count)
        };
    }
    JSON.stringify(data);
    return data;
}

function getBarChartLabel(content) {
    var data = [];
    for (i = 0; i < content.length; i++) {
        data[i] = content[i].territorial.name;
    }
    JSON.stringify(data);
    return data;
}

function getBarChartData(content) {
    var data = [];
    for (i = 0; i < content.length; i++) {
        data[i] = content[i].count;
    }
    JSON.stringify(data);
    return data;
}