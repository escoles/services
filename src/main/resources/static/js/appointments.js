$(document).ready(function () {
    var urlSpeciality = $('#urlSpeciality').val();
    var urlAppointments = $('#urlAppointments').val();
    var urlDayType= $('#urlDayType').val();

    var dataTableSpeciality = $('#speciality-datatable').DataTable({
        serverSide: true,
        pageLength: 25,
        responsive: true,
        ordering: false,
        searching: true,
        dom: 'Tlp<"html5buttons"B><"bottom"gitp>i',
        lengthMenu: [10, 25, 50],
        language: {
            url: '/localization/es_CA.json'
        },
        buttons: [
            {extend: 'excel'},
            {extend: 'copy'},
            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        ajax: {
            contentType: 'application/json',
            url: urlSpeciality,
            type: 'GET',
            data: function (d) {
                d.speciality = $("#speciality").val();
                d.season = $("#season").val();
            },
            dataFilter: function (data) {
                var json = {};
                var jsonData = jQuery.parseJSON(data);
                json.recordsTotal = jsonData.length;
                json.recordsFiltered = jsonData.length;
                json.data = contentToSpecialityData(jsonData.content); // data.result
                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {"data": "speciality"},
            {"data": "lastNumber"},
            {"data": "maxNumber"}
        ]
    });

    var dataTableAppointment = $('#appointments-datatable').DataTable({
        serverSide: true,
        processing: true,
        pageLength: 25,
        responsive: true,
        ordering: false,
        searching: false,
        dom: 'Tflp<"html5buttons"B><"bottom"gitp>i',
        lengthMenu: [10, 25, 50],
        language: {
            url: '/localization/es_CA.json'
        },
        buttons: [
            {extend: 'excel'},
            {extend: 'copy'},
            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        ajax: {
            contentType: 'application/json',
            url: urlAppointments,
            type: 'GET',
            data: function (d) {
                d.speciality = $("#speciality").val();
                d.season = $("#season").val();
            },
            dataFilter: function (data) {
                var json = {};
                var jsonData = jQuery.parseJSON(data);
                json.recordsTotal = jsonData.totalElements;
                json.recordsFiltered = jsonData.totalElements;
                json.data = contentToAppointmentData(jsonData.content); // data.result
                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {"data": "number"},
            {"data": "date"},
            {"data": "centerName"},
            {"data": "speciality"},
            {"data": "dayType"},
            {"data": "init"},
            {"data": "end"}
        ]
    });

    var dataTableDayType = $('#dayType-datatable').DataTable({
        serverSide: true,
        pageLength: 25,
        responsive: true,
        ordering: false,
        searching: true,
        dom: 'Tlp<"html5buttons"B><"bottom"gitp>i',
        lengthMenu: [10, 25, 50],
        language: {
            url: '/localization/es_CA.json'
        },
        buttons: [
            {extend: 'excel'},
            {extend: 'copy'},
            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        ajax: {
            contentType: 'application/json',
            url: urlDayType,
            type: 'GET',
            data: function (d) {
                d.speciality = $("#speciality").val();
                d.season = $("#season").val();
                d.daytype = $("#dayType").val();
            },
            dataFilter: function (data) {
                var json = {};
                var jsonData = jQuery.parseJSON(data);
                json.recordsTotal = jsonData.length;
                json.recordsFiltered = jsonData.length;
                json.data = contentToSpecialityData(jsonData.content); // data.result
                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {"data": "speciality"},
            {"data": "dayType"},
            {"data": "maxNumber"}
        ]
    });

    $(document).on("change", "#specialityTypeHead", function (e) {
        var current = $(this).typeahead("getActive");
        if ($(this).val().length===0)
            $("#speciality").val('');
        else
            $("#speciality").val(current.id);
        dataTableSpeciality.ajax.reload();
        dataTableAppointment.ajax.reload();
        dataTableDayType.ajax.reload();
    });

    $(document).on("click", "#resetSpeciality", function (e) {
        $("#specialityTypeHead").val('');
        $("#speciality").val('');
        dataTableSpeciality.ajax.reload();
        dataTableAppointment.ajax.reload();
        dataTableDayType.ajax.reload();
    });

    $(document).on("change", "#season", function (e) {
        dataTableSpeciality.ajax.reload();
        dataTableAppointment.ajax.reload();
        dataTableDayType.ajax.reload();
    });

    $(document).on("change", "#dayType", function (e) {
        dataTableDayType.ajax.reload();
    });

    $.get("/api/v1/speciality", function (data, status) {
        if (status === 'success') {
            $("#specialityTypeHead").typeahead({ source:data });
        }
    });

});


function contentToAppointmentData(content) {
    var data = [];
    for (i = 0; i < content.length; i++) {
        data[i] = {
            "id": content[i].id,
            "number": formatNumber(content[i].number),
            "date": getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].date)))),
            "centerName": content[i].centerName,
            "speciality": content[i].speciality.name,
            "dayType": getDayType(content[i].dayType),
            "init": getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].init)))),
            "end": getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].end)))),
            "territorial": content[i].territorial
        };
    }
    return data;
}

function contentToSpecialityData(content) {
    var data = [];
    for (i = 0; i < content.length; i++) {
        var specialityId = "";
        var specialityName = "";
        var dayType = content[i].dayType;
        if (content[i].speciality){
            specialityId = content[i].speciality.id;
            specialityName = content[i].speciality.name;
        }
        data[i] = {
            "speciality": specialityId + " - " + specialityName,
            "maxNumber": formatNumber(content[i].data.maxNumber) + " ("
            + getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].data.maxDate)))) + ")",
            "lastNumber": formatNumber(content[i].data.lastNumber) + " ("
            + getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].data.lastDate)))) + ")",
            "dayType": getDayType(dayType)
        };
    }
    return data;
}

