var dataTable;

$(document).ready(function () {

    var url = $('#url').val();
    dataTable = $('#actions-datatable').DataTable({
        serverSide: true,
        processing: true,
        responsive: true,
        ordering: false,
        searching: false,
        dom: 'Tflp<"html5buttons"B><"bottom"gitp>i',
        lengthMenu: [5, 10, 15],
        pageLength: 10,
        language: {
            url: '/localization/es_CA.json'
        },
        buttons: [
            {extend: 'excel'},
            {extend: 'copy'},
            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        ajax: {
            contentType: 'application/json',
            url: '/api/v1/action',
            type: 'GET',
            data: function (d) {
                d.term = $("#term").val();
            },
            dataFilter: function (data) {
                var json = {};
                var jsonData = jQuery.parseJSON(data);
                json.recordsTotal = jsonData.totalElements;
                json.recordsFiltered = jsonData.totalElements;
                json.data = contentToActionData(jsonData.content); // data.result
                return JSON.stringify(json); // return JSON string
            }
        },
        columns: [
            {"data": "date"},
            {"data": "text"},
            {"data": "hasComments"},
            {
                "data": "text",
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    var date = new Date(JSON.parse(JSON.stringify(oData.date)));
                    var html = "<button onclick=javascript:removeAction(\"" + oData.id + "\") class=\"btn btn-sm btn-primary pull-right m-t-n-xs\"><strong>Esborrar</strong></button>";
                    $(nTd).html(html);
                }
            }


        ]
    });

    $(document).on("change keyup paste", "#term", function (e) {
        if ($(this).val().length===0 || $(this).val().length>3)
            dataTable.ajax.reload();
    });

    $('#actions-datatable tbody').on('dblclick', 'tr', function () {
        var data = dataTable.row(this).data();
        if (data.comments && data.comments.length > 0) {
            var comments = '';
            for (i = 0; i < data.comments.length; i++) {
                comments = comments + data.comments[i] + "<br>";
            }
            var commentsModalContent = document.getElementById('commentsModalContent');
            commentsModalContent.innerHTML = comments;
            $('#commentsModal').modal('show');
        }
        // Marquem la notificació com ha llegida
        $.post("/api/v1/action/read/"+data.id, function (data, status) {
            if (status === 'success') {
                dataTable.ajax.reload();
            }
        });

    });
});

function getDateFormat(date){
    year = "" + date.getFullYear();
    month = "" + (date.getMonth() + 1); if (month.length === 1) { month = "0" + month; }
    day = "" + date.getDate(); if (day.length === 1) { day = "0" + day; }
    hour = "" + date.getHours(); if (hour.length === 1) { hour = "0" + hour; }
    minute = "" + date.getMinutes(); if (minute.length === 1) { minute = "0" + minute; }
    second = "" + date.getSeconds(); if (second.length === 1) { second = "0" + second; }
    return day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
}


function contentToActionData(content) {
    var data = [];
    for (i = 0; i < content.length; i++) {
        data[i] = {
            "id": content[i].id,
            "date": formatIsRead(content[i], getDateAsString(new Date(JSON.parse(JSON.stringify(content[i].date))))),
            "text": formatIsRead(content[i], content[i].text),
            "hasComments": formatIsRead(content[i], (content[i].comments && content[i].comments.length > 0? "Si": "No")),
            "comments": content[i].comments
        };
    }
    return data;
}

function formatIsRead(oData, text) {
    if (oData.read)
        return text;
    else
        return "<strong>" + text + "</strong>"
}

function removeAction(id) {
    // Eliminem la notificació
    $.ajax({
        url: "/api/v1/action/"+id,
        type: 'DELETE',
        success: function(response) {
            dataTable.ajax.reload();
        }
    });
}