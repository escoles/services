toastr.options = {
    closeButton: false,
    progressBar: true,
    showMethod: 'slideDown',
    timeOut: 4000
};


$(document).ready(function () {
    // Mostrem notificació per informar de les accions pendents de revisar
    $.get("/api/v1/action/read", function (data, status) {
        if (status === 'success') {
            var span = document.getElementById('actionsSpan');
            if (data>0) {
                span.innerHTML = data;
                toastr.info('', 'Hi ha ' + data + ' accions pendents de revisar');
            }
            // TODO: CONTROLAR SEGONS COOCKIE DIARIA
        }

    });


});

