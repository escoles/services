package cat.jaumemoron.centers.services.controller;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.constants.RequestMappingConstants;
import cat.jaumemoron.centers.services.util.FilesUtilsForTests;
import cat.jaumemoron.centers.services.util.UstecMockServer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class AdminControllerTest extends UstecMockServer {

    @Autowired
    private AdminController controller;

    @Value("${ustec.file.repository}")
    private String repository;

    @Test
    public void initImportCenters() {
        String path = controller.initImportCenters();
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_CENTER_REQUEST_MAPPING);
    }

    @Test
    public void importCenters() throws IOException {
        Model model = new ConcurrentModel();
        MultipartFile multipartFile = FilesUtilsForTests.createMultipartFile("filename", "text");
        String path = controller.importCenters(multipartFile, model);
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_CENTER_REQUEST_MAPPING);
        assertTrue(model.containsAttribute("result"));
    }

    @Test
    public void importCentersFails()  {
        Model model = new ConcurrentModel();
        try {
            controller.importCenters(null, model);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }

    }

    @Test
    public void initImportTypes() {
        String path = controller.initImportTypes();
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_TYPES_REQUEST_MAPPING);
    }

    @Test
    public void importTypes() throws IOException {
        Model model = new ConcurrentModel();
        MultipartFile multipartFile = FilesUtilsForTests.createMultipartFile("filename", "text");
        String path = controller.importTypes(multipartFile, model);
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_TYPES_REQUEST_MAPPING);
        assertTrue(model.containsAttribute("result"));
    }

    @Test
    public void initImportAppointments() {
        String path = controller.initImportAppointments();
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_APPOINTMENT_REQUEST_MAPPING);
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void importAppointments() throws IOException {
        Model model = new ConcurrentModel();
        MultipartFile multipartFile = FilesUtilsForTests.createMultipartFile("filename", "text");
        String path = controller.importAppointments(multipartFile, model);
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.IMPORT_APPOINTMENT_REQUEST_MAPPING);
        assertTrue(model.containsAttribute("result"));
    }

    @Test
    public void action() {
        String path = controller.action();
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.ACTION_REQUEST_MAPPING);
    }

    @Test
    public void initLoadAppointments() {
        String path = controller.initLoadAppointments();
        assertEquals(path, RequestMappingConstants.ADMIN +
                RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING +
                RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING);
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void loadAppointments() {
        Model model = new ConcurrentModel();
        String path = controller.loadAppointments(new Date(), null, model);
        assertEquals(path, RequestMappingConstants.ADMIN +
                RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING +
                RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING);
        assertTrue(model.containsAttribute("result"));
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void loadAppointments1() {
        Date date = new Date();
        String url = "http://localhost:9999/nomenaments/nous";
        Model model = new ConcurrentModel();
        String path = controller.loadAppointments(date, url, model);
        assertEquals(path, RequestMappingConstants.ADMIN +
                RequestMappingConstants.LOAD_APPOINTMENT_REQUEST_MAPPING +
                RequestMappingConstants.APPOINTMENT_REQUEST_MAPPING);
        assertTrue(model.containsAttribute("result"));
    }

    @Test
    public void getFiles() throws IOException {
        Path path = Paths.get(repository);
        if (!path.toFile().exists())
            Files.createDirectory(path);
        Model model = new ConcurrentModel();
        String response = controller.getFiles(model);
        assertEquals(response, RequestMappingConstants.ADMIN + RequestMappingConstants.FILE_REQUEST_MAPPING);
        assertTrue(model.containsAttribute("files"));
    }

    @Test
    public void getFile() throws IOException {
        Path path = Paths.get(repository);
        if (!path.toFile().exists())
            Files.createDirectory(path);
        File file = new File(repository + "/file.test");
        file.deleteOnExit();
        Path f = Files.createFile(file.toPath());
        f.toFile().deleteOnExit();
        ;
        String id = "file.test";
        MockHttpServletResponse response = new MockHttpServletResponse();
        controller.getFile(id, response);
    }

    @Test
    public void getFileFails() throws IOException {
        String id = "XXX.test";
        MockHttpServletResponse response = new MockHttpServletResponse();
        try {
            controller.getFile(id, response);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void statistics() {
        String path = controller.statistics();
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.STATISTIC_REQUEST_MAPPING);
    }

    @Test
    public void getSocial() {
        String path = controller.getSocial();
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.SOCIAL_REQUEST_MAPPING);
    }


    @Test
    public void postSocial() {
        Model model = new ConcurrentModel();
        String path = controller.postSocial(null, null, model);
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.SOCIAL_REQUEST_MAPPING);
        path = controller.postSocial("text", "text", model);
        assertEquals(path, RequestMappingConstants.ADMIN + RequestMappingConstants.SOCIAL_REQUEST_MAPPING);
    }
}
