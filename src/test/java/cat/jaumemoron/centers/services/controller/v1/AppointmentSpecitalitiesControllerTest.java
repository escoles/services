package cat.jaumemoron.centers.services.controller.v1;

import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.controller.api.v1.SpecialitiesController;
import cat.jaumemoron.centers.services.dto.SpecialityDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class AppointmentSpecitalitiesControllerTest {

    @Autowired
    private SpecialitiesController controller;

    @Test
    public void findAll() {
        ResponseEntity<List<SpecialityDTO>> list = controller.findAll();
        assertNotNull(list);
        assertNotNull(list.getBody());
        assertFalse(list.getBody().isEmpty());
    }
}
