package cat.jaumemoron.centers.services.controller;

import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.dto.ErrorResponseDTO;
import cat.jaumemoron.centers.services.exception.ResourceNotFoundException;
import cat.jaumemoron.centers.services.exception.RestExceptionHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class RestExceptionHandlerTest {

    @Autowired
    private RestExceptionHandler handler;

    @Test
    public void handleResourceNotFoundException() {
        String message = "message";
        ResourceNotFoundException exception = new ResourceNotFoundException(message);
        ResponseEntity<ErrorResponseDTO> response = handler.handleResourceNotFoundException(exception);
        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
        assertNotNull(response.getBody());
        assertEquals(response.getBody().getStatus(), HttpStatus.NOT_FOUND);
        assertEquals(response.getBody().getMessage(), "Entity not found");
        assertFalse(response.getBody().getErrors().isEmpty());
        assertEquals(response.getBody().getErrors().get(0), message);
        assertTrue(response.getBody().getTimestamp() > 0);
    }

    @Test
    public void handleIllegalArgumentException() {
        String message = "message";
        IllegalArgumentException exception = new IllegalArgumentException(message);
        ResponseEntity<ErrorResponseDTO> response = handler.handleIllegalArgumentException(exception);
        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertNotNull(response.getBody());
        assertEquals(response.getBody().getStatus(), HttpStatus.BAD_REQUEST);
        assertEquals(response.getBody().getMessage(), "Wrong parameter error");
        assertFalse(response.getBody().getErrors().isEmpty());
        assertEquals(response.getBody().getErrors().get(0), message);
        assertTrue(response.getBody().getTimestamp() > 0);
    }

    @Test
    public void handleException() {
        String message = "message";
        Exception exception = new Exception(message);
        ResponseEntity<ErrorResponseDTO> response = handler.handleException(exception);
        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        assertNotNull(response.getBody());
        assertEquals(response.getBody().getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        assertEquals(response.getBody().getMessage(), "Unknown error");
        assertFalse(response.getBody().getErrors().isEmpty());
        assertEquals(response.getBody().getErrors().get(0), message);
        assertTrue(response.getBody().getTimestamp() > 0);
    }


}
