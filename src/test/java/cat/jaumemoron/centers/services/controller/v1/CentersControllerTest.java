package cat.jaumemoron.centers.services.controller.v1;

import cat.jaumemoron.centers.persistence.domain.Center;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.controller.api.v1.CentersController;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class CentersControllerTest {

    @Autowired
    private CentersController controller;

    public void get() {
        String id = "LL1";
        Center dto = controller.get(id);
        assertNotNull(dto);
    }

    @Test
    public void getNotfound() {
        String id = "XXX";
        try {
            controller.get(id);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(ResourceNotFoundException.class, e.getClass());
        }
    }

    @Test
    public void getIsNull() {
        try {
            controller.get(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void getIsEmpty() {
        try {
            controller.get("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findAll() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        PageDTO<Center> result = controller.findAll(page, size, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test()
    public void findAllFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 10;
        int size = 1;
        PageDTO<Center> result = controller.findAll(page, size, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);
    }

    @Test
    public void findByTerm() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String term = "vella";
        PageDTO<Center> result = controller.findAll(page, size, term, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 1L);
    }

    @Test
    public void findByTermFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        try {
            controller.findAll(page, size, "X", UriComponentsBuilder.newInstance(), response);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }

    }

    @Test
    public void findAllFails1() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        PageDTO<Center> result = controller.findAll(page, size, "", UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test
    public void findAllFails2() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        PageDTO<Center> result = controller.findAll(page, size, "1234", UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);
    }

    @Test
    public void findByType() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String type = "EINF1C";
        PageDTO<Center> result = controller.findByType(type, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test
    public void findByTypeAndCity() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String type = "EINF1C";
        String city = "1";
        PageDTO<Center> result = controller.findByTypeAndCity(type, city, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test()
    public void findByTypeAndCityFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String type = "EINF1C";
        String city = "XXXX";
        PageDTO<Center> result = controller.findByTypeAndCity(type, city, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);
    }

    @Test
    public void findByTypeAndRegion() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String type = "EINF1C";
        String region = "1";
        PageDTO<Center> result = controller.findByTypeAndRegion(type, region, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test
    public void findByCity() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String city = "1";
        PageDTO<Center> result = controller.findByCity(city, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test()
    public void findByCityFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String city = "X";
        PageDTO<Center> result = controller.findByCity(city, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);
    }

    @Test
    public void findByRegion() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String region = "1";
        PageDTO<Center> result = controller.findByRegion(region, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test()
    public void findByRegionFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String region = "X";
        PageDTO<Center> result = controller.findByRegion(region, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);
    }

    @Test
    public void findByName() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String name = "Maria";
        PageDTO<Center> result = controller.findByName(name, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test
    public void findByNameFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String name = "X";
        try {
            controller.findByName(name, page, size, UriComponentsBuilder.newInstance(), response);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }

    }

    @Test()
    public void findByNameFails1() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String name = "XXXXXX";
        PageDTO<Center> result = controller.findByName(name, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);
    }

    @Test
    public void findByLocation() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        double latitude = 41.418204100;
        double longitude = 2.181243680;
        int distance = 5;
        int page = 0;
        int size = 2;
        PageDTO<Center> result = controller.findByLocation(latitude, longitude, distance, page, size,
                UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 1L);
    }

    @Test
    public void findByTerritorial() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String territorial = "1";
        PageDTO<Center> result = controller.findByTerritorial(territorial, page, size, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test
    public void findByTerritorialAndTerm() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String territorial = "1";
        PageDTO<Center> result = controller.findByTerritorial(territorial, page, size, "prim", UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2);
    }

    @Test()
    public void findByTerritorialFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String territorial = "X";
        PageDTO<Center> result = controller.findByTerritorial(territorial, page, size, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);

    }

    @Test
    public void findByNatureCity() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String nature = "1";
        PageDTO<Center> result = controller.findByNature(nature, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test()
    public void findByNatureFails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String nature = "X";
        PageDTO<Center> result = controller.findByNature(nature, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);

    }

    @Test
    public void findByOwner() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String owner = "1";
        PageDTO<Center> result = controller.findByOwner(owner, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 2L);
    }

    @Test()
    public void findByOwnerails() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String owner = "X";
        PageDTO<Center> result = controller.findByOwner(owner, page, size, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(result.getNumberOfElements(), 0L);
    }
}
