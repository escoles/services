package cat.jaumemoron.centers.services.controller.v1;

import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.domain.Appointment;
import cat.jaumemoron.centers.persistence.domain.AppointmentData;
import cat.jaumemoron.centers.persistence.domain.AppointmentDataDayType;
import cat.jaumemoron.centers.persistence.dto.NumberDataDTO;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.controller.api.v1.AppointmentsController;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.exception.ResourceNotFoundException;
import cat.jaumemoron.centers.services.util.UstecMockServer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class AppointmentsControllerTest extends UstecMockServer {

    @Autowired
    private AppointmentsController controller;

    @Test
    public void findByTerritorial() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String season = "2017-2018";
        String territorial = "0508";
        PageDTO<Appointment> result = controller.findByTerritorial(territorial, page, size, season, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(2L, result.getNumberOfElements());
    }

    @Test
    public void findByTerritoriaNotFound() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String territorial = "X";
        String season = "2017-2018";
        PageDTO<Appointment> result = controller.findByTerritorial(territorial, page, size, season, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(0L, result.getNumberOfElements());
    }

    @Test
    public void findByTerritorialSpecialityNotFound() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 2;
        String territorial = "0508";
        String season = "2017-2018";
        PageDTO<Appointment> result = controller.findByTerritorial(territorial, page, size, season, "a", UriComponentsBuilder.newInstance(), response);
        assertNotNull(result);
        assertEquals(0L, result.getNumberOfElements());
    }

    @Test
    public void findById() {
        String id = "1";
        Appointment appointment = controller.get(id);
        assertNotNull(appointment);
        assertEquals(appointment.getId(), id);
        assertEquals(appointment.getTerritorial().getId(), "0508");
    }

    @Test
    public void findByIdIsNUll() {
        try {
            controller.get(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByIdIsEmpty() {
        try {
            controller.get("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByIdNotFound() {
        try {
            controller.get("XX");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(ResourceNotFoundException.class, e.getClass());
        }
    }

    @Test()
    public void findByTerritorialSpeciality() {
        int page = 0;
        int size = 5;
        String season = "2000-2001";
        String territorialId = "0308";
        String speciality = "133";
        PageDTO<AppointmentData> result = controller.findByTerritorialSpeciality(territorialId, page, size, season, null);
        assertNotNull(result);
        assertEquals(3L, result.getNumberOfElements());
        result = controller.findByTerritorialSpeciality(territorialId, page, size, season, speciality);
        assertNotNull(result);
        assertEquals(3L, result.getNumberOfElements());
    }

    @Test
    public void findByTerritorialSpecialityDayType() {
        int page = 0;
        int size = 5;
        String territorial = "0308";
        String season = "2000-2001";
        PageDTO<AppointmentDataDayType> result = controller.findByTerritorialSpecialityDayType(territorial, page, size, season, null, null);
        assertEquals(3L, result.getNumberOfElements());
        result = controller.findByTerritorialSpecialityDayType(territorial, page, size, season, DayType.FULL, "133");
        assertEquals(1L, result.getNumberOfElements());
        result = controller.findByTerritorialSpecialityDayType(territorial, page, size, season, DayType.FULL, null);
        assertTrue(result.getNumberOfElements()>=0);
        result = controller.findByTerritorialSpecialityDayType(territorial, page, size, season, null, "133");
        assertTrue(result.getNumberOfElements()>=0);
    }

    @Test
    public void getNumberData() {
        String territorialId = "0508";
        String specialityId = "192";
        long number = 1000;
        long number2 = 998;
        List<NumberDataDTO> list = controller.getAnalytics(territorialId, specialityId, number);
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(4, list.size());
    }

    @Test
    public void findByTerritorialSpeciality2() {
        String season = "2017-2018";
        String speciality = "192";
        PageDTO<AppointmentData> page = controller.findByTerritorialSpeciality(season, speciality);
        assertNotNull(page);
        assertEquals(0L, page.getNumberOfElements());
    }
}

