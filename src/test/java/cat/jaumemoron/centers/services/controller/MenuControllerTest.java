package cat.jaumemoron.centers.services.controller;


import cat.jaumemoron.centers.services.ServicesApplicationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class MenuControllerTest {

    @Autowired
    private MenuController controller;

    @Autowired
    private Environment environment;


    @Test
    public void territorial() {
        Model model = new ConcurrentModel();
        String response = controller.territorial("0508", model);
        assertEquals(response, "territorial");
    }

    @Test
    public void territorialNotFound() {
        Model model = new ConcurrentModel();
        String response = controller.territorial("XXXX", model);
        assertEquals(response, "error");
    }

    @Test
    public void appointment() {
        Model model = new ConcurrentModel();
        String response = controller.appointment("0508", model);
        assertEquals(response, "appointment");
    }

    @Test
    public void appointmentNotFound() {
        Model model = new ConcurrentModel();
        String response = controller.appointment("XXXX", model);
        assertEquals(response, "error");
    }

    @Test
    public void isGoogleAnalyticsActive() {
        boolean isGoogleAnalyticsActive = controller.isGoogleAnalyticsActive();
        if (Arrays.asList(environment.getActiveProfiles()).contains("production"))
            assertTrue(isGoogleAnalyticsActive);
        else
            assertFalse(isGoogleAnalyticsActive);
    }

    @Test
    public void home() {
        Model model = new ConcurrentModel();
        String response = controller.home(model);
        assertEquals(response, "home");
        assertTrue(model.containsAttribute("seasonList"));
    }

    @Test
    public void centerList() {
        String response = controller.centerList();
        assertEquals(response, "centers");
    }

    @Test
    public void centerDistance() {
        String response = controller.centerDistance();
        assertEquals(response, "distance");
    }

    @Test
    public void login() {
        String response = controller.login();
        assertEquals(response, "login");
    }

    @Test
    public void faqs() {
        String response = controller.faqs();
        assertEquals(response, "faqs");
    }

    @Test
    public void links() {
        String response = controller.links();
        assertEquals(response, "links");
    }

    @Test
    public void appointments() {
        String response = controller.appointments();
        assertEquals(response, "appointments");
    }

    @Test
    public void cookies() {
        String response = controller.cookies();
        assertEquals(response, "cookies");
    }

    @Test
    public void analytics() {
        String response = controller.analytics();
        assertEquals(response, "analytics");
    }

    @Test
    public void speciality() {
        Model model = new ConcurrentModel();
        String response = controller.speciality(model);
        assertEquals(response, "speciality");
    }
}
