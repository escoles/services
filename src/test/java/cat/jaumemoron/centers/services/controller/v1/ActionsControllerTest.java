package cat.jaumemoron.centers.services.controller.v1;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.Action;
import cat.jaumemoron.centers.persistence.service.ActionService;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.controller.api.v1.ActionsController;
import cat.jaumemoron.centers.services.dto.PageDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class ActionsControllerTest {

    @Autowired
    private ActionsController controller;

    @Autowired
    private ActionService service;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void get() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        int page = 0;
        int size = 10;
        // Obtenim la llista d'elements actual
        PageDTO<Action> pageDto = controller.findAll(page, size, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(pageDto);
        long totalElements = pageDto.getTotalElements();
        // Creem un registre per poder recuperar-lo després
        // Creem l'entitat a bdd
        Action action = Action.of("actionName", new Date());
        action = service.create(action);
        // Comprobem que s'ha creat
        pageDto = controller.findAll(page, size, "", UriComponentsBuilder.newInstance(), response);
        assertNotNull(pageDto);


        pageDto = controller.findAll(page, size, "nname", UriComponentsBuilder.newInstance(), response);
        assertNotNull(pageDto);
        assertEquals(pageDto.getTotalElements(), 1L);

        long read = controller.countUnRead();

        // Marquem l'element com a llegit
        controller.markAsRead(action.getId());

        // Eliminem l'acció creada
        controller.delete(action.getId());
        ;
        pageDto = controller.findAll(page, size, null, UriComponentsBuilder.newInstance(), response);
        assertNotNull(pageDto);
        assertTrue(pageDto.getTotalElements() >= totalElements);

    }


}
