package cat.jaumemoron.centers.services.controller.v1;

import cat.jaumemoron.centers.persistence.domain.City;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.controller.api.v1.CitiesController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class CitiesControllerTest {

    @Autowired
    private CitiesController controller;

    @Test
    public void get() {
        ResponseEntity<List<City>> cityList = controller.findAll();
        assertNotNull(cityList);
        assertNotNull(cityList.getBody());
        assertFalse(cityList.getBody().isEmpty());
    }


}
