package cat.jaumemoron.centers.services.controller;

import cat.jaumemoron.centers.services.ServicesApplicationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class CacheControllerTest {

    @Autowired
    private CacheController controller;

    @Test
    public void get() {
        Collection<String> cacheList = controller.get();
        assertNotNull(cacheList);
        assertFalse(cacheList.isEmpty());
        for (String cache : cacheList) {
            controller.clear(cache);
        }
        controller.all();
    }
}
