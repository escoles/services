package cat.jaumemoron.centers.services.controller.v1;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import cat.jaumemoron.centers.persistence.service.StatisticsService;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.controller.api.v1.StatisticsController;
import cat.jaumemoron.centers.services.dto.PageDTO;
import cat.jaumemoron.centers.services.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class StatisticsControllerTest {

    @Autowired
    private StatisticsController controller;

    @Autowired
    private StatisticsService service;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByType() throws ParseException {
        // Creem registres de prova
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Statistic s1 = new Statistic();
        s1.setDate(sdf.parse("02/01/2018"));
        s1.setType(StatisticType.TELEGRAM_REQUEST);
        s1.setNumberOfOks(1L);
        s1.setNumberOfErrors(0L);
        Statistic s2 = new Statistic();
        s2.setDate(sdf.parse("05/01/2018"));
        s2.setType(StatisticType.TELEGRAM_REQUEST);
        s2.setNumberOfOks(0L);
        s2.setNumberOfErrors(1L);
        service.save(s1);
        service.save(s2);
        int page = 0;
        int size = 10;
        int type = 0;

        // Obtenim la llista d'elements actual
        PageDTO<Statistic> pageDto = controller.findByType(type, null, null, page, size);
        assertNotNull(pageDto);
        assertEquals(pageDto.getTotalElements(), 2L);

        pageDto = controller.findByType(type, sdf.parse("01/01/2018"), sdf.parse("01/02/2018"), page, size);
        assertNotNull(pageDto);
        assertEquals(pageDto.getTotalElements(), 2L);

        pageDto = controller.findByType(type, sdf.parse("01/01/2018"), sdf.parse("04/01/2018"), page, size);
        assertNotNull(pageDto);
        assertEquals(pageDto.getTotalElements(), 1);

        service.remove(StatisticType.TELEGRAM_REQUEST);

    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByTypeFails() {
        try {
            controller.findByType(-1, null, null, 0, 2);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByTypeFails1() {
        try {
            controller.findByType(10, null, null, 0, 2);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

}
