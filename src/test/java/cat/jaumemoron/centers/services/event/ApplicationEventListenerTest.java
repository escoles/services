package cat.jaumemoron.centers.services.event;

import cat.jaumemoron.centers.persistence.service.AppointmentService;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import cat.jaumemoron.centers.services.component.SpringTemplateComponent;
import cat.jaumemoron.centers.services.telegram.TelegramCommandBot;
import cat.jaumemoron.centers.services.twitter.TwitterService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class ApplicationEventListenerTest {

    @InjectMocks
    private ApplicationEventListener listener;

    @Mock
    private TwitterService twitterService;

    @Mock
    private TelegramCommandBot bot;

    @Mock
    private SpringTemplateComponent springTemplateComponent;

    @Mock
    private AppointmentService appointmentService;

    @Test
    public void handleSocialEvent() {
        listener.setTelegramEnabled(true);
        listener.setTwitterEnabled(true);
        doNothing().when(twitterService).updateStatus(anyString());
        doNothing().when(bot).sendMessageToGroup(ArgumentMatchers.any());
        when(springTemplateComponent.getText(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn("text");
        listener.handleSocialEvent(new SocialEvent("Text"));
        listener.setTelegramEnabled(false);
        listener.setTwitterEnabled(false);
        listener.handleSocialEvent(new SocialEvent("Text2"));
    }

    @Test
    public void getTwitterText() {
        String text1 = "text1";
        String text2 = "text2";
        List<String> list = new ArrayList<>();
        list.add(text1);
        list.add(text2);
        when(springTemplateComponent.getTwitterText(ArgumentMatchers.any())).thenReturn(list);
        when(appointmentService.getTerritorialsLastNumber(ArgumentMatchers.any())).thenReturn(null);
        List<String> textList = listener.getTwitterText();
        assertNotNull(textList);
        assertFalse(textList.isEmpty());
        assertEquals(textList.size(), 2);
        assertTrue(textList.get(0).contains(text1));
        assertTrue(textList.get(1).contains(text2));
    }

    @Test
    public void canSendMessage() {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;
        assertTrue(listener.canSendMessage(LocalTime.parse("10:00:00", formatter)));
        assertTrue(listener.canSendMessage(LocalTime.parse("11:30:00", formatter)));
        assertTrue(listener.canSendMessage(LocalTime.parse("12:00:00", formatter)));
        assertTrue(listener.canSendMessage(LocalTime.parse("19:00:00", formatter)));
        assertFalse(listener.canSendMessage(LocalTime.parse("01:00:00", formatter)));
        assertFalse(listener.canSendMessage(LocalTime.parse("06:59:00", formatter)));
        assertFalse(listener.canSendMessage(LocalTime.parse("20:59:59", formatter)));


    }
}
