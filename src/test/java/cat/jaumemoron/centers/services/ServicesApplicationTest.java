package cat.jaumemoron.centers.services;

import cat.jaumemoron.centers.persistence.configuration.EnablePersistenceConfiguration;
import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.persistence.repository.*;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.client.NodeClientFactoryBean;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Date;
import java.util.UUID;

@EnablePersistenceConfiguration
@SpringBootApplication
public class ServicesApplicationTest {

    @Autowired
    private ElasticsearchOperations operations;

    @Autowired
    private CentersRepository centersRepository;

    @Autowired
    private RegionsRepository regionRepository;

    @Autowired
    private CitiesRepository cityRepository;

    @Autowired
    private NaturesRepository natureRepository;

    @Autowired
    private OwnersRepository ownerRepository;

    @Autowired
    private TypesRepository typesRepository;

    @Autowired
    private AppointmentDataRepository appointmentDataRepository;

    @Autowired
    private AppointmentDataDayTypeRepository appointmentDataDayTypeRepository;

    @Bean
    public NodeClientFactoryBean client() {
        NodeClientFactoryBean bean = new NodeClientFactoryBean(false);
        bean.setClusterName(UUID.randomUUID().toString());
        bean.setEnableHttp(false);
        bean.setPathData("target/elasticsearch/data");
        bean.setPathHome("target/elasticsearch/home");
        return bean;
    }

    @Bean
    public ElasticsearchTemplate elasticsearchTemplate(Client client) throws Exception {
        return new ElasticsearchTemplate(client);
    }

    @PostConstruct
    public void insertDataForTest() {
        // Inserim dades de prova de comarques
        typesRepository.save(Type.of("EINF1C", "Educació infantil de 1r cicle"));
        typesRepository.save(Type.of("EINF2C", "Educació infantil de 2n cicle"));
        typesRepository.save(Type.of("EPRI", "Educació primària"));
        typesRepository.save(Type.of("EE", "Educació especial"));
        typesRepository.save(Type.of("ESO", "Educació secundària obligatòria"));
        typesRepository.save(Type.of("BATX", "Batxillerat"));
        typesRepository.save(Type.of("AA01", "Curs d'accés als cicles formatius de grau mitjà"));
        typesRepository.save(Type.of("CFPM", "Cicles formatius professionals de grau mitjà CFPM"));
        typesRepository.save(Type.of("PPAS", "Curs de preparació a la prova d'accés de cicles formatius professionals de grau superior"));
        typesRepository.save(Type.of("AA02", "Curs d'accés als cicles formatius de grau superior"));
        typesRepository.save(Type.of("CFPS", "Cicles formatius professionals de grau superior"));
        typesRepository.save(Type.of("PFI", "Programes de formació i inserció"));
        typesRepository.save(Type.of("CFAM", "Cicles formatius d'arts plàstiques i disseny de grau mitjà"));
        typesRepository.save(Type.of("CFAS", "Cicles formatius d'arts plàstiques i disseny de grau superior"));
        typesRepository.save(Type.of("ESDI", "Ensenyaments superiors de disseny ESDL"));
        typesRepository.save(Type.of("ADR", "Ensenyaments superiors d'art dramàtic"));
        typesRepository.save(Type.of("CRBC", "Ensenyaments superiors de conservació i restauració de bens culturals"));
        typesRepository.save(Type.of("IDI", "Ensenyaments d'idiomes"));
        typesRepository.save(Type.of("DANE", "Escoles de dansa (no reglat)"));
        typesRepository.save(Type.of("DANC", "Grau professional de dansa"));
        typesRepository.save(Type.of("DANS", "Grau superior de dansa"));
        typesRepository.save(Type.of("MUSE", "Escoles de música (no reglat)"));
        typesRepository.save(Type.of("CUSC", "Grau professional de música"));
        typesRepository.save(Type.of("MUSS", "Grau superior de música"));
        typesRepository.save(Type.of("TEGM", "Tècnics esportius de grau mitjà"));
        typesRepository.save(Type.of("TEGS", "Tècnics esportius de grau superior"));
        typesRepository.save(Type.of("ESTR", "Ensenyaments estrangers"));
        typesRepository.save(Type.of("ADULTS", "Formació de persones adultes"));

        regionRepository.save(Region.of("1", "Barcelonès"));
        regionRepository.save(Region.of("2", "Tarragona"));
        regionRepository.save(Region.of("3", "Urgell"));
        // Inserim dades de prova de cities
        cityRepository.save(City.of("1", "Barcelona"));
        cityRepository.save(City.of("2", "Reus"));
        cityRepository.save(City.of("3", "Guimerà"));

        // Inserim dades de prova de cities
        natureRepository.save(Nature.of("1", "nature"));
        natureRepository.save(Nature.of("2", "Públic"));
        natureRepository.save(Nature.of("3", "Privat"));

        // Inserim dades de prova de cities
        ownerRepository.save(Owner.of("1", "owner"));
        ownerRepository.save(Owner.of("2", "owner1"));
        ownerRepository.save(Owner.of("3", "Departament d'Ensenyament"));
        // Inserim dades de prova de centers
        centersRepository.save(Center.Builder.aCenter()
                .withId("LL1")
                .withName("Llar Infants Maria la vella")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withType(Type.of("EPRI", "Educació primària"))
                .withAddress("Via LL 1")
                .withPostalCode("08001")
                .withCity(City.of("1", "Barcelona"))
                .withRegion(Region.of("1", "Barcelonès"))
                .withLocation(41.416021597, 2.101776153)
                .withTerritorial(Territorial.of("2", "Tarragona"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "Departament d'Ensenyament"))
                .build());
        centersRepository.save(Center.Builder.aCenter()
                .withId("LL2")
                .withName("Llar Infants 1")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withAddress("Via LL 2")
                .withPostalCode("08002")
                .withCity(City.of("1", "Barcelona"))
                .withRegion(Region.of("1", "Barcelonès"))
                .withLocation(41.40021642, 22.200944109)
                .withTerritorial(Territorial.of("2", "Tarragona"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "Departament d'Ensenyament"))
                .build());
        centersRepository.save(Center.Builder.aCenter()
                .withId("LL3")
                .withName("Llar Infants Cosa Nostra")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withAddress("Via LL 3")
                .withPostalCode("08003")
                .withCity(City.of("2", "Reus"))
                .withRegion(Region.of("2", "Tarragon"))
                .withLocation(41.145998101, 1.098460174)
                .withTerritorial(Territorial.of("1", "Consorci d'Educació de Barcelona"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "owner"))
                .build());
        centersRepository.save(Center.Builder.aCenter()
                .withId("PRI1")
                .withName("Santa Maria")
                .withType(Type.of("EPRI", "Educació primària"))
                .withAddress("Via PRI 1")
                .withPostalCode("08004")
                .withCity(City.of("1", "Barcelona"))
                .withRegion(Region.of("1", "Barcelonès"))
                .withLocation(41.418204098, 2.181243681)
                .withTerritorial(Territorial.of("1", "Consorci d'Educació de Barcelona"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "owner"))
                .build());
        centersRepository.save(Center.Builder.aCenter()
                .withId("PRI2")
                .withName("Escola 1")
                .withType(Type.of("EPRI", "Educació primària"))
                .withAddress("Via PRI 2")
                .withPostalCode("08005")
                .withCity(City.of("1", "Barcelona"))
                .withRegion(Region.of("1", "Barcelonès"))
                .withLocation(41.380893104, 2.138574748)
                .withTerritorial(Territorial.of("1", "Consorci d'Educació de Barcelona"))
                .withNature(Nature.of("1", "Privat"))
                .withOwner(Owner.of("1", "Departament d'Ensenyament"))
                .build());
        centersRepository.save(Center.Builder.aCenter()
                .withId("PRI3")
                .withName("Centre Primer 3")
                .withType(Type.of("EPRI", "Educació primària"))
                .withAddress("Via PRI 3")
                .withPostalCode("08005")
                .withCity(City.of("3", "Guimerà"))
                .withRegion(Region.of("3", "Urgell"))
                .withLocation(41.561752471, 1.185315112)
                .withTerritorial(Territorial.of("1", "Consorci d'Educació de Barcelona"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "Departament d'Ensenyament"))
                .build());

        centersRepository.save(Center.Builder.aCenter()
                .withId("43010062")
                .withName("CFA El Vendrell")
                .withType(Type.of("EPRI", "Educació primària"))
                .withAddress("Via PRI 3")
                .withPostalCode("08005")
                .withCity(City.of("3", "Guimerà"))
                .withRegion(Region.of("3", "Urgell"))
                .withLocation(41.561752471, 1.185315112)
                .withTerritorial(Territorial.of("0143", "Tarragona"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "Departament d'Ensenyament"))
                .build());

        centersRepository.save(Center.Builder.aCenter()
                .withId("PRI5")
                .withName("AFA Vila-seca")
                .withType(Type.of("EPRI", "Educació primària"))
                .withAddress("Via PRI 3")
                .withPostalCode("08005")
                .withCity(City.of("3", "Guimerà"))
                .withRegion(Region.of("3", "Urgell"))
                .withLocation(41.561752471, 1.185315112)
                .withTerritorial(Territorial.of("0143", "Tarragona"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "Departament d'Ensenyament"))
                .build());

        centersRepository.save(Center.Builder.aCenter()
                .withId("PRI6")
                .withName("Escola - Mogent")
                .withType(Type.of("EPRI", "Educació primària"))
                .withAddress("Via PRI 3")
                .withPostalCode("08005")
                .withCity(City.of("3", "Guimerà"))
                .withRegion(Region.of("3", "Urgell"))
                .withLocation(41.561752471, 1.185315112)
                .withTerritorial(Territorial.of("0508", "Maresme - Vallès Oriental", "5"))
                .withNature(Nature.of("1", "Públic"))
                .withOwner(Owner.of("1", "Departament d'Ensenyament"))
                .build());

        AppointmentData data1 = new AppointmentData();
        data1.setSeason("2000-2001");
        data1.setTerritorial(AppointmentTerritorial.of("0308", "Baix Llobregat"));
        data1.setSpeciality(AppointmentSpeciality.of("133", "Alemany Eoi"));
        data1.setData(AppointmentNumberData.of(1000, new Date()));
        AppointmentData data2 = new AppointmentData();
        data2.setSeason("2000-2001");
        data2.setTerritorial(AppointmentTerritorial.of("0308", "Baix Llobregat"));
        data2.setSpeciality(AppointmentSpeciality.of("133", "Alemany Eoi"));
        data2.setData(AppointmentNumberData.of(2000, new Date()));
        AppointmentData data3 = new AppointmentData();
        data3.setSeason("2000-2001");
        data3.setTerritorial(AppointmentTerritorial.of("0308", "Baix Llobregat"));
        data3.setSpeciality(AppointmentSpeciality.of("133", "Àrab"));
        data3.setData(AppointmentNumberData.of(3000, new Date()));
        AppointmentDataDayType data4 = new AppointmentDataDayType();
        data4.setSeason("2000-2001");
        data4.setTerritorial(AppointmentTerritorial.of("0308", "Baix Llobregat"));
        data4.setSpeciality(AppointmentSpeciality.of("133", "Alemany Eoi"));
        data4.setData(AppointmentNumberData.of(4000, new Date()));
        data4.setDayType(DayType.FULL);
        AppointmentDataDayType data5 = new AppointmentDataDayType();
        data5.setSeason("2000-2001");
        data5.setTerritorial(AppointmentTerritorial.of("0308", "Baix Llobregat"));
        data5.setSpeciality(AppointmentSpeciality.of("133", "Alemany Eoi"));
        data5.setData(AppointmentNumberData.of(5000, new Date()));
        data5.setDayType(DayType.A_THIRD);
        AppointmentDataDayType data6 = new AppointmentDataDayType();
        data6.setSeason("2000-2001");
        data6.setTerritorial(AppointmentTerritorial.of("0308", "Baix Llobregat"));
        data6.setSpeciality(AppointmentSpeciality.of("133", "Àrab"));
        data6.setData(AppointmentNumberData.of(6000, new Date()));
        ;
        data6.setDayType(DayType.A_THIRD);
        appointmentDataRepository.save(data1);
        appointmentDataRepository.save(data2);
        appointmentDataRepository.save(data3);
        appointmentDataDayTypeRepository.save(data4);
        appointmentDataDayTypeRepository.save(data5);
        appointmentDataDayTypeRepository.save(data6);

    }
}
