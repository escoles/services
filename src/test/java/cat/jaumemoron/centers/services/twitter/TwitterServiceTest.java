package cat.jaumemoron.centers.services.twitter;

import cat.jaumemoron.centers.services.ServicesApplicationTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import twitter4j.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class TwitterServiceTest {

    @Value("${social.twitter.appId}")
    private String appId;

    @Value("${social.twitter.appSecret}")
    private String appSecret;

    @Value("${social.twitter.token}")
    private String token;

    @Value("${social.twitter.tokenSecret}")
    private String tokenSecret;

    @InjectMocks
    private TwitterService service;

    @Mock
    private TwitterFactory twitterFactory;

    @BeforeTestClass
    public void setUp() {
        service.setAppId(appId);
        service.setAppSecret(appSecret);
        service.setToken(token);
        service.setTokenSecret(tokenSecret);
    }

    @Test
    public void updateStatus() throws TwitterException {
        Twitter twitter = Mockito.mock(Twitter.class);
        when(twitterFactory.getInstance()).thenReturn(twitter);
        doNothing().when(twitter).setOAuthConsumer(ArgumentMatchers.any(), ArgumentMatchers.any());
        doNothing().when(twitter).setOAuthAccessToken(ArgumentMatchers.any());
        Status status = Mockito.mock(Status.class);
        when(status.getInReplyToStatusId()).thenReturn(0L);
        when(twitter.updateStatus(ArgumentMatchers.anyString())).thenReturn(status);
        when(twitter.updateStatus(ArgumentMatchers.any(StatusUpdate.class))).thenReturn(status);
        List<String> tweetList = new ArrayList<>();
        tweetList.add("Tweet 1");
        tweetList.add("Tweet 2");
        service.updateStatus(tweetList);
    }
}
