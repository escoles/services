package cat.jaumemoron.centers.services.component;

import cat.jaumemoron.centers.persistence.domain.AppointmentTerritorial;
import cat.jaumemoron.centers.persistence.dto.TerritorialCountDTO;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class SpringTemplateComponentTest {

    @Autowired
    private SpringTemplateComponent component;

    @Test
    public void getTelegramText() {
        String text = component.getTelegramText();
        assertNotNull(text);
        assertTrue(text.contains("Tenim nova informació sobre nomenaments"));
    }

    @Test
    public void getTwitterText() {
        AppointmentTerritorial appointmentTerritorial = AppointmentTerritorial.of("0108", "TERRITORIAL");
        List<TerritorialCountDTO> appointmentDataList = new ArrayList<>();
        appointmentDataList.add(TerritorialCountDTO.Builder.aTerritorialCountDTO()
                .withTerritorial(appointmentTerritorial)
                .withCount(25000).build());
        List<String> textList = component.getTwitterText(appointmentDataList);
        assertNotNull(textList);
        assertFalse(textList.isEmpty());
        assertEquals(textList.size(), 2);
        assertTrue(textList.get(0).contains("Tenim nova informació sobre nomenaments."));
        assertTrue(textList.get(1).contains("L´últim nomenament de la territorial TERRITORIAL és el 25.000"));
        assertTrue(textList.get(1).contains("https://www.mestremurri.cat/appointment/territorial/0108"));

    }
}
