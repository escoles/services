package cat.jaumemoron.centers.services.component;

import cat.jaumemoron.centers.persistence.domain.AppointmentData;
import cat.jaumemoron.centers.persistence.domain.AppointmentNumberData;
import cat.jaumemoron.centers.persistence.domain.AppointmentSpeciality;
import cat.jaumemoron.centers.services.ServicesApplicationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServicesApplicationTest.class)
public class TemplatesTest {

    @Autowired
    private SpringTemplateEngine springTemplateEngine;

    @Test
    public void territorialData() {
        List<AppointmentData> data = new ArrayList<>();
        AppointmentData data1 = new AppointmentData();
        AppointmentData data2 = new AppointmentData();
        data1.setSpeciality(AppointmentSpeciality.of("id", "name"));
        data1.setData(AppointmentNumberData.of(21000, new Date()));
        data1.getData().setLastNumber(22000);
        data2.setSpeciality(AppointmentSpeciality.of("id2", "name"));
        data2.setData(AppointmentNumberData.of(23000, new Date()));
        data2.getData().setLastNumber(24000);
        data.add(data1);
        data.add(data2);
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("territorialName", "name");
        attributes.put("specialityData", data);
        Context ctx = new Context(Locale.ENGLISH);
        ctx.setVariables(attributes);
        String template = "telegram/territorialData.html";
        String text = springTemplateEngine.process(template, ctx);
        System.out.println(text);
        assertNotNull(text);
        assertFalse(text.isEmpty());
    }
}
