package cat.jaumemoron.centers.services.util;

import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.services.dto.PageDTO;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ConverterTest {

    @Test
    public void toCenter() {
        Center c1 = Center.Builder.aCenter()
                .withId("LL1")
                .withName("Llar Infants Maria la vella")
                .withType(Type.of("EINF1C", "EINF1C"))
                .withAddress("Via LL 1")
                .withPostalCode("08001")
                .withCity(City.of("!", "Barcelona"))
                .withRegion(Region.of("1", "Barcelonès"))
                .withPhone("telefon1")
                .withEmail("email1")
                .withEmail("email2")
                .withOwner(Owner.of("1", "owner1"))
                .withNature(Nature.of("1", "nature1"))
                .withTerritorial(Territorial.of("1", "territorial1"))
                .withLocation(41.416021597, 2.101776153)
                .build();
        Center c2 = Center.Builder.aCenter()
                .withId("LL2")
                .withName("Llar Infants 1")
                .withType(Type.of("EINF1C", "EINF1C"))
                .withAddress("Via LL 2")
                .withPostalCode("08002")
                .withCity(City.of("1", "Barcelona"))
                .withRegion(Region.of("1", "Barcelonès"))
                .withPhone("telefon1")
                .withPhone("telefon2")
                .withEmail("email1")
                .withOwner(Owner.of("1", "owner1"))
                .withNature(Nature.of("1", "nature1"))
                .withTerritorial(Territorial.of("1", "territorial1"))
                .withLocation(41.40021642, 22.200944109)
                .build();
        List<Center> list = new ArrayList<>();
        list.add(c1);
        list.add(c2);
        PageRequest request = PageRequest.of(0, 2);
        long total = 10;
        PageImpl<Center> page = new PageImpl<Center>(list, request, total);
        PageDTO<Center> pageDTO = Converter.toCenterPageDTO(page);
        assertNotNull(pageDTO);
        assertEquals(pageDTO.getNumberOfElements(), 2L);
        assertEquals(pageDTO.getTotalElements(), total);
        assertEquals(pageDTO.getTotalPages(), (int) total / request.getPageSize());
        assertEquals(pageDTO.getContent().size(), list.size());
        assertEquals(pageDTO.getContent().get(0).getId(), c1.getId());
        assertEquals(pageDTO.getContent().get(0).getName(), c1.getName());
        assertEquals(pageDTO.getContent().get(0).getTypeList(), c1.getTypeList());
        assertEquals(pageDTO.getContent().get(0).getAddress(), c1.getAddress());
        assertEquals(pageDTO.getContent().get(0).getPostalCode(), c1.getPostalCode());
        assertEquals(pageDTO.getContent().get(0).getCity(), c1.getCity());
        assertEquals(pageDTO.getContent().get(0).getRegion(), c1.getRegion());
        assertEquals(pageDTO.getContent().get(0).getPhoneList(), c1.getPhoneList());
        assertEquals(pageDTO.getContent().get(0).getEmailList(), c1.getEmailList());
        assertEquals(pageDTO.getContent().get(0).getFax(), c1.getFax());
        assertEquals(pageDTO.getContent().get(0).getLocation(), c1.getLocation());
        assertEquals(pageDTO.getContent().get(1).getId(), c2.getId());
        assertEquals(pageDTO.getContent().get(1).getName(), c2.getName());
        assertEquals(pageDTO.getContent().get(1).getTypeList(), c2.getTypeList());
        assertEquals(pageDTO.getContent().get(1).getAddress(), c2.getAddress());
        assertEquals(pageDTO.getContent().get(1).getPostalCode(), c2.getPostalCode());
        assertEquals(pageDTO.getContent().get(1).getCity(), c2.getCity());
        assertEquals(pageDTO.getContent().get(1).getRegion(), c2.getRegion());
        assertEquals(pageDTO.getContent().get(1).getPhoneList(), c2.getPhoneList());
        assertEquals(pageDTO.getContent().get(1).getEmailList(), c2.getEmailList());
        assertEquals(pageDTO.getContent().get(1).getFax(), c2.getFax());
        assertEquals(pageDTO.getContent().get(1).getLocation(), c2.getLocation());
        assertEquals(pageDTO.getContent().get(1).getTerritorial(), c2.getTerritorial());
        assertEquals(pageDTO.getContent().get(1).getNature(), c2.getNature());
        assertEquals(pageDTO.getContent().get(1).getOwner(), c2.getOwner());
    }
}
