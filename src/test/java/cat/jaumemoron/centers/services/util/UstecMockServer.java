package cat.jaumemoron.centers.services.util;

import cat.jaumemoron.centers.services.component.AppointmentSchedulerComponentTest;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class UstecMockServer {


    private static WireMockServer wireMockServer = new WireMockServer(9999);

    @AfterAll
    public static void stopJiraServer() {
        wireMockServer.stop();
    }

    @BeforeAll
    public static void configJiraServer() throws URISyntaxException, IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");


        byte[] appointmentsFile = Files.readAllBytes(Paths.get(AppointmentSchedulerComponentTest.class.getResource("/files/appointments.html").toURI()));
        wireMockServer.start();
        wireMockServer.stubFor(
                get(urlEqualTo("/nomenaments/avui/?data=" + sdf.format(new Date())))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html")
                                .withBody(new String(appointmentsFile))));
        wireMockServer.start();
        wireMockServer.stubFor(
                get(urlEqualTo("/nomenaments/nous"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html; charset=UTF-8")
                                .withBody(new String(Files.readAllBytes(Paths.get(AppointmentSchedulerComponentTest.class.getResource("/files/appointments.json").toURI()))))));
    }
}
