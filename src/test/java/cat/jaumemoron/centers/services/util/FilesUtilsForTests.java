package cat.jaumemoron.centers.services.util;

import org.hamcrest.MatcherAssert;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FilesUtilsForTests {

    public static MultipartFile createMultipartFile(String filename, String text) throws IOException {
        File file = createTempFile(filename, text);
        MatcherAssert.assertThat(file, is(not(nullValue())));
        assertTrue(file.exists());
        return new MockMultipartFile("file", filename, "text/plain", text.getBytes());
    }

    public static File createTempFile(String filename, String text) throws IOException {
        Files.write(Paths.get("./" + File.separatorChar + filename), text.getBytes());
        File file = new File("./" + File.separatorChar + filename);
        file.deleteOnExit();
        return file;
    }

    public static MultipartFile createMultipartFromFile(String name, String path) throws IOException {
        assertNotNull(path);
        byte[] text = Files.readAllBytes(Paths.get(path));
        return new MockMultipartFile("file", name, "text/plain", text);
    }

}
