package cat.jaumemoron.centers.services.util;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EncoderTest {

    @Test
    public void encode() {
        String password = "admin";
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String result = encoder.encode(password);
        System.out.println(result);
        assertTrue(encoder.matches(password, result));
    }


}
