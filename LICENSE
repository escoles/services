                  Llicència Pública General de GNU
                         Versió 3, 29 de juny de 2007

Drets d'autor (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
Es permet la còpia i distribució literal d'aquest document de llicència,
però no es permet la seva modificació.

                               Preàmbul

La Llicència Pública General de GNU és una llicència lliure, de copyleft,
per a programari i altres tipus d'obres.

Les llicències de la majoria del programari i altres obres pràctiques estan
dissenyades per eliminar la teva llibertat per compartir i canviar les obres.
En canvi, la Llicència Pública General de GNU està destinada a garantir la teva
llibertat per compartir i canviar totes les versions d'un programa, assegurant-se
que aquest roman lliure per a tots els seus usuaris. La Free Software Foundation,
nosaltres, utilitzem la Llicència Pública General de GNU per a la majoria del
nostre programari; també s'aplica a qualsevol altra obra alliberada d'aquesta manera
pels seus autors. Podeu aplicar-la als vostres programes també.

Quan parlem de programari lliure, ens referim a la llibertat, no al preu.
Les nostres Llicències Públiques Generals estan dissenyades per assegurar que
tens la llibertat de distribuir còpies de programari lliure (i cobrar-hi si ho desitges),
que rebeu el codi font o podeu obtenir-lo si ho desitgeu, que podeu canviar el programari
o utilitzar-ne peces en nous programes lliures, i que sabeu que podeu fer aquestes coses.

Per protegir els teus drets, necessitem evitar que altres et neguin aquestes llibertats o
et demanin que les abandonis. Per tant, tens certes responsabilitats si distribueixes còpies
del programari o si el modifiques: responsabilitats per respectar la llibertat dels altres.

Per exemple, si distribuïu còpies d'un programa, ja sigui gratuïtament o a canvi d'un pagament,
heu de transmetre als destinataris les mateixes llibertats que vau rebre. Heu de vetllar perquè
ells també rebin o puguin obtenir el codi font. I heu de mostrar-los aquestes condicions perquè
sàpiguen els seus drets.

Els desenvolupadors que utilitzen la GPL protegeixen els teus drets amb dos passos: (1) afirmar
el dret d'autor sobre el programari i (2) oferir-te aquesta llicència per donar-te permís legal
per copiar, distribuir i/o modificar-lo.

Per a la protecció dels desenvolupadors i autors, la GPL explica clarament que no hi ha cap garantia
per a aquest programari lliure. Tant per als usuaris com per als autors, la GPL requereix que
les versions modificades estiguin marcats com a modificats, perquè els seus problemes no siguin
atribuïts erròniament als autors de les versions anteriors.

Alguns dispositius estan dissenyats per negar als usuaris l'accés per instal·lar o executar
versions modificades del programari dins ells, tot i que el fabricant pot fer-ho. Això és
fonamentalment incompatible amb l'objectiu de protegir la llibertat dels usuaris de canviar
el programari. El patró sistemàtic d'aquest abús es produeix a l'àrea de productes per a
usuaris individuals, que és precisament on és més inacceptable. Per tant, hem dissenyat aquesta
versió de la GPL per prohibir aquesta pràctica per a aquests productes. Si aquests problemes
sorgeixen substancialment en altres àmbits, estem preparats per ampliar aquesta disposició a
aquests àmbits en versions futures de la GPL, si cal per protegir la llibertat dels usuaris.

Finalment, cada programa està constantment amenaçat per patents de programari.
Els estats no haurien de permetre que les patents restringeixin el desenvolupament i
ús de programari en ordinadors de propòsit general, però en aquells que ho fan,
volem evitar el perill especial que les patents aplicades a un programa lliure
puguin fer-lo efectivament propietari. Per evitar això, la GPL assegura que
les patents no es puguin utilitzar per fer que el programa no sigui lliure.

Els termes i condicions precisos per a la còpia, distribució i modificació segueixen.

                   TERMES I CONDICIONS

  0. Definicions.

  "Aquesta Llicència" es refereix a la versió 3 de la Llicència Pública General de GNU.

  "Drets d'autor" també significa lleis semblants als drets d'autor que s'apliquen a
altres tipus d'obres, com ara les màscares de semiconductors.

  "El Programa" es refereix a qualsevol obra amb drets d'autor llicenciada sota aquesta
Llicència. Cada llicenciat es tracta com "tu". "Llicenciats" i "destinataris" poden ser
individus o organitzacions.

  "Modificar" una obra significa copiar-ne o adaptar-ne tot o part d'una manera que requereixi
el permís dels drets d'autor, diferent de fer una còpia exacta. El treball resultant es diu
"versió modificada" de l'obra anterior o una obra "basada en" l'obra anterior.

  Una "obra coberta" significa ja sigui el Programa sense modificar o una obra basada en el
Programa.

  "Propagar" una obra significa fer qualsevol cosa amb ella que, sense permís, et faria responsable
directa o indirectament d'infringir la llei de drets d'autor aplicable, excepte executar-la en
un ordinador o modificar una còpia privada. La propagació inclou la còpia, la distribució
(amb o sense modificació), posar-la a disposició del públic i, en alguns països, altres
activitats també.

  "Transmetre" una obra significa qualsevol tipus de propagació que permeti a altres
parts fer o rebre còpies. La simple interacció amb un usuari a través d'una xarxa d'ordinadors,
sense transferència d'una còpia, no és considerada transmissió.

  Una interfície d'usuari interactiva mostra "Notificacions Legals Apropiades" en la
mesura que inclogui una característica convenient i clarament visible que (1) mostri un
avís de drets d'autor apropiat, i (2) informi l'usuari que no hi ha cap garantia per a
l'obra (excepte en la mesura que s'ofereixin garanties), que els llicenciats poden
transmetre l'obra sota aquesta llicència, i com visualitzar una còpia d'aquesta Llicència.
Si la interfície presenta una llista de comandes o opcions de l'usuari, com ara un menú,
un element destacat en la llista compleix aquest criteri.

  1. Codi Font.

  El "codi font" d'una obra significa la forma preferida de la mateixa per fer-ne
modificacions. El "codi objecte" significa qualsevol forma no font d'una obra.

  Una "Interfície Estàndard" significa una interfície que sigui una norma oficial
definida per una organització de normalització reconeguda, o, en el cas d'interfícies
especificades per a un llenguatge de programació particular, una que sigui àmpliament
utilitzada entre els desenvolupadors que treballen en aquest llenguatge.

  Les "Biblioteques del Sistema" d'una obra executiva inclouen qualsevol cosa, tret de
l'obra com a conjunt, que (a) estigui inclosa en la forma normal d'empaquetament d'un
Component Principal, però que no sigui part d'aquest Component Principal, i (b) serveixi
només per permetre l'ús de l'obra amb aquest Component Principal, o per implementar una
Interfície Estàndard per a la qual hi hagi una implementació disponible per al públic
en forma de codi font. Un "Component Principal", en aquest context, significa un component
esencial important (núcli, sistema de finestres, etc.) del sistema operatiu específic
(si n'hi ha algun) en què s'executa l'obra executiva, o un compilador utilitzat per produir
l'obra, o un intèrpret de codi objecte utilitzat per executar-lo.

  El "Codi Font Correspondent" per a una obra en forma de codi objecte significa tot el codi
font
