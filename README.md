# Què és Mestremurri.cat

És una web que obté que pretén facilitar la vida als mestre interins obtenint i processant tota la informació dels nomenaments telemàtics del Departament d'Educació de la Generalitat de Catalunya.

Els mestres interins necessiten aquesta informació i la manera en que s'oferia, bé des de les webs de l'administració (http://gencat.cat i http://xtec.cat) o des de la web de l'Ustec era molt farragosa:  un pdf amb tots els nomenaments o un llistat simple.

Amb la web mestremurri.cat vam voler facilitar la vida als mestres processant aquesta informació i donant-li valor: filtrant per territorial o especialitat, veure els nomenaments d'altres anys o cercant un número i sabent quan havia estat nomenat els altres anys.

# On es troba el codi font?

El codi font es troba dins del d'un grup de projectes de [Gitlab] (https://gitlab.com/escoles)

Mestremurri es composa de 3 projectes Java gestionats amb Maven organitzats de la següent manera:

* **parent**: És on conté totes les dependències i les versions de les mateixes que s'han de fer servir
* **persistence**: Aquest projecte conté tota la capa de persistència de l'aplicació, és a dir, totes les entitats, repositoris i serveis necessaris per treballar amb les diferents fonts de dades
* **services**: És una aplicació Spring Boot que conté la pròpia web i els processos automàtics per a`la obtenció de dades i la publicació automomàtica de novetats a xarxes socials.

A continuació, llistarem algunes de les funcionalitats de l'aplicació:

## Home

![home1](./docs/img/home1.png)
![home2](./docs/img/home2.png)

## Nomenaments

### Menú principal
![nomenamants1](./docs/img/nomenamants.png)
### Nomenaments d'una territorial
![nomenamants2](./docs/img/nomenamants2.png)
### Nomenaments d'una territorial agrupats per especialitat
![nomenamants3](./docs/img/nomenamants3.png)
### Nomenaments d'una territorial agrupats per jornada
![nomenamants4](./docs/img/nomenamants4.png)
### Especialitats
Indicant una especialitat es mostrarà una gràfica que mostraràm, per a cada territorial, quans nomenaments hi ha hagut d'aquesta especialitat 
![especialitats](./docs/img/especialitats.png)
### El menu número
Indicant un número, es mostra quan va sortir nomenant els anteriors anys. D'aquesta manera, un pot fer-se una idea de quan pot sortir el seu número.
![elmeunumero](./docs/img/elmeunumero.png)

# Llicència

Tot el codi font està sota la llicència Pública General de GNU (en anglès, "GPL"), per tant, és dona completa llibertat als usuaris per a utilitzar, modificar i distribuir el programari segons les condicions d'aquesta llicència.
